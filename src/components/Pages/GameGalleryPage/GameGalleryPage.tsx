import React, { FC } from 'react';
import { Link } from 'react-router-dom';

import GameIndex from 'components/Games';

import classes from './GameGalleryPage.module.css';

const GameGalleryPage: FC = () => {
    return (
        <div className={classes.Container}>
            {Object.entries(GameIndex).map(([gameName, gameData]) => {
                return (
                    <div 
                        key={gameName}
                        className={classes.GameIcon} 
                        style={{backgroundImage: `url(${gameData.image})`, backgroundColor: '#000'}}
                    >

                        <Link to={`/game/${gameName}`}>{gameData.name}</Link>
                    </div>
                )
            })}
        </div>
    )
};

export default GameGalleryPage;
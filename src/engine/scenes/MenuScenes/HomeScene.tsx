
import { forwardRef, useCallback, useContext, useEffect, useImperativeHandle, useRef, useState } from 'react';
import Logo from 'engine/gameobjects/Menu/Logo';
import * as T3 from 'three';
import { SceneProps } from 'engine/EngineScene';
import { ThemeContext } from 'components/App/App';

interface HomeSceneProps extends SceneProps {
    assets: {
        logoModel: T3.Group,
        logoTexture: T3.Texture,
        backgroundTexture: T3.Texture,
        logoShaders: [string, string]
    }
}

const HomeScene = forwardRef(({canvas, renderer, assets}: HomeSceneProps, ref) => {

    let [scene, setScene] = useState<T3.Scene | null>(null);
    let [camera, setCamera] = useState<T3.Camera | null>(null);
    let [logo, setLogo] = useState<Logo | null>(null);
    let [bgMesh, setBgMesh] = useState<T3.Points | null>(null);
    let [elapsedTime, setElapsedTime] = useState<number>(0);

    const [normalisedMousePos, setNormalisedMousePos] = useState<[number, number]>([0.5, 0.5]);
    
    let canvasRect = useRef(canvas.getBoundingClientRect());

    const themeContext = useContext(ThemeContext);

    const handleMouseMove = useCallback((mouseEvent: MouseEvent & {layerX?: number, layerY?: number}) => {
        if(mouseEvent.layerX && mouseEvent.layerY){
            setNormalisedMousePos([
                mouseEvent.layerX/canvasRect.current.width,
                mouseEvent.layerY/canvasRect.current.height
            ]);
        }else{
            setNormalisedMousePos([
                (mouseEvent.clientX - canvasRect.current.left) / canvasRect.current.width, 
                (mouseEvent.clientY - canvasRect.current.top) / canvasRect.current.height
            ]);
        }
    }, []);
    

    useEffect(() => {
        if(canvas){
            const newScene = new T3.Scene();
            const newCamera = new T3.PerspectiveCamera(50, canvas.width/canvas.height, 0.1, 200);

            const width = 10;
            const height = 10;

            setScene(newScene);
            setCamera(newCamera);   
            setLogo(new Logo(
                newScene, 
                canvas, 
                assets.logoModel,
                assets.logoTexture,
                assets.backgroundTexture,
                assets.logoShaders,
                themeContext.value === 'dark'? 0x000000: 0xFFFFFF
            ));

            let seperation = 0.035;

            let numParticlesX = Math.floor(width/seperation) + 1;
            let numParticlesY = Math.floor(height/seperation) + 1;

            let numParticles = numParticlesX * numParticlesY;

            let positions = new Float32Array(numParticles * 3);

            let i = 0;

            for ( let ix = 0; ix < numParticlesX; ix ++ ) {
                for ( let iy = 0; iy < numParticlesY; iy ++ ) {

                    positions[ i ] = ix * seperation - ( ( numParticlesX * seperation ) / 2 ); // x
                    positions[ i + 1 ] = iy * seperation - ( ( numParticlesY * seperation ) / 2 ); // y
                    positions[ i + 2 ] = 1;

                    i += 3;
                }
            }

            let geometry = new T3.BufferGeometry();
            geometry.setAttribute('position', new T3.BufferAttribute(positions, 3));

            let uniforms = {
                time: {value: 0},
                texture1: {type: "t", value: assets.backgroundTexture}
            }

            const bgPlaneMaterial = new T3.ShaderMaterial( {
                uniforms,
                vertexShader: `
                    uniform sampler2D texture1;
                    uniform float time; 
                    varying float vScale;

                    void main() {

                        vec4 fragCoord = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                        fragCoord.xyz /= fragCoord.w;
                        fragCoord.w = 1.0 / fragCoord.w;
                        fragCoord.xyz *= vec3(0.5); 
                        fragCoord.xyz += vec3(0.5); 

                        vec4 color = texture2D(texture1, fragCoord.xy);
                        float animationFactor = (sin(time/3.) + 1.0) / 2.0;
                        float scale = mix(color.r, color.b, smoothstep(0.0, 0.5, animationFactor));
                        scale = mix(scale, color.g, smoothstep(0.5, 1.0, animationFactor));

                        vScale = 7.0 * scale;
                        gl_PointSize = vScale;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                    }
                `,
                fragmentShader: `
                    uniform sampler2D texture1;
                    varying float vScale;

                    vec3 white = vec3(0.8, 0.8, 0.8);

                    void main() {
                        if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;
                        gl_FragColor = vec4(white, 1.);
                    }
                `
            });

            let bgMesh_init = new T3.Points( geometry, bgPlaneMaterial );
            newScene.add( bgMesh_init );
            setBgMesh(bgMesh_init);
            
            newCamera.position.set(0, 0, -5);
            newCamera.lookAt(0, 0, 0);

            const ambientLight = new T3.AmbientLight(0xffffff, 0.001);
            newScene.add(ambientLight);

            document.addEventListener('mousemove', handleMouseMove);
        }
    }, [
        canvas,
        handleMouseMove, 
        themeContext,
        assets.logoModel,
        assets.logoTexture,
        assets.backgroundTexture,
        assets.logoShaders
    ]);

    useImperativeHandle(ref, () => ({update}));

    const update = (deltaTime: number) : void => {

        if(elapsedTime + deltaTime > 300){
            setElapsedTime(0)
        }else{
            setElapsedTime(elapsedTime + deltaTime);
        }

        if(bgMesh){
            (bgMesh.material as T3.ShaderMaterial).uniforms.time.value = elapsedTime;
            (bgMesh.material as T3.ShaderMaterial).needsUpdate = true;
        }

        if(logo){
            logo.setRotation(normalisedMousePos);
        }
        if(scene && camera){
            renderer.render(scene, camera);
        }
    }

    return null;
});

export default HomeScene;
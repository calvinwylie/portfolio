import fragment from './point.frag';
import vertex from './point.vert';

export default {
    fragment,
    vertex
}
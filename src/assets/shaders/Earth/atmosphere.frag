struct DirectionalLight {
    vec3 color;
    vec3 direction;
};

varying vec2 vUv;
varying vec4 vWSNormal;
varying vec4 vNormal;
varying vec4 vPosition; 

float edge = 0.8;

uniform DirectionalLight directionalLights[1];


void main() {
    vec3 L = normalize(directionalLights[0].direction);
    vec3 WSN = normalize(vWSNormal.xyz);
    vec3 N = normalize(vNormal.xyz);
    vec3 C = normalize(cameraPosition);
    float perp = dot(WSN, C);
    perp = 1. - smoothstep(0.6, 1.1, perp);

    float intensity;

    if(perp < edge){
        intensity = 0.2 + pow((0.9/edge) * perp, 2.0);
    }else{
        intensity = pow(((1.0/(1.0 - edge) * perp) - 1./(1. - edge)), 2.0);
    }

    float lightIntensity = dot(N, L) * 2.;
    lightIntensity = smoothstep(-0.2, 0.25, lightIntensity);

    float step1 = 0.1;
    float step2 = 0.95;

    intensity *= lightIntensity;
    // black -> dark blue
    vec4 color = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(0.05, 0.4, 0.6, intensity), smoothstep(0.0, step1, intensity));
    // -> light blue
    color = mix(color, vec4(0.3, 0.65, 1., intensity), smoothstep(step1, step2, intensity));
    // -> off white
    color = mix(color, vec4(0.85, 0.95, 1., intensity), smoothstep(step2, 1.0, intensity)); 

    gl_FragColor = color;
}
             
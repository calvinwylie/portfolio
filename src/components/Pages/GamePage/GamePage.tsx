import React, {FC, Suspense} from 'react';
import { useParams } from 'react-router-dom';

import LoadingPage from '../LoadingPage/LoadingPage';
import GameIndex from 'components/Games';

import classes from './GamePage.module.css';


const GamePage: FC = () => {

    const params = useParams();

    if(params.game && GameIndex[params.game]){
        const GameComponent = GameIndex[params.game].component;
        return (
            <div className={classes.Container}>
                <Suspense fallback={<LoadingPage/>}>
                    <GameComponent/>
                </Suspense>
            </div>
        )
    }else{
        return null;
    }
};

export default GamePage;
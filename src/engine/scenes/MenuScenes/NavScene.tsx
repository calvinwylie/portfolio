
import { forwardRef, useCallback, useContext, useEffect, useImperativeHandle, useRef } from 'react';
import * as T3 from 'three';
import { SceneProps } from 'engine/EngineScene';
import { ThemeContext } from 'components/App/App';

interface MenuScene extends SceneProps {}
interface ComponentVariables {
    time: number;
    data: Float32Array;
    scene: T3.Scene;
    camera: T3.Camera;
    
    canvasRect: DOMRect;
    numParticles: number;

    particleMaterial: T3.ShaderMaterial | null;
    particleGeometry: T3.PlaneGeometry | null;
    particleMesh: T3.Mesh | null;

    fbo: T3.WebGLRenderTarget | null;
    fbo2: T3.WebGLRenderTarget | null;

    compPlaneGeom: T3.PlaneGeometry | null;
    compCamera: T3.OrthographicCamera;
    compScene: T3.Scene;
    compMaterial: T3.ShaderMaterial | null;
    compMesh: T3.Mesh | null;
    compTexture: T3.DataTexture | null;
}

function createRenderTarget(width: number, height: number): T3.WebGLRenderTarget{
    return new T3.WebGLRenderTarget(
            width, height, 
            {
                minFilter: T3.NearestFilter,
                magFilter: T3.NearestFilter,
                format: T3.RGBAFormat,
                type: T3.FloatType
            }
        )
}

const SIZE = 128;

const NavScene = forwardRef(({canvas, renderer, assets}: MenuScene, ref) => {

    let componentsVariables = useRef<ComponentVariables>({
        time: 0,
        data: new Float32Array(SIZE * SIZE * 4),
        scene: new T3.Scene(),
        camera: new T3.PerspectiveCamera(50, canvas.width/canvas.height, 0.1, 200),

        canvasRect: canvas.getBoundingClientRect(),
        numParticles: SIZE*SIZE,

        particleMaterial: null,
        particleGeometry: null,
        particleMesh: null,

        fbo: createRenderTarget(SIZE, SIZE),
        fbo2: createRenderTarget(SIZE, SIZE),

        compPlaneGeom: new T3.PlaneGeometry(2, 2),
        compCamera: new T3.OrthographicCamera(-1, 1, 1, -1, -1, 1),
        compScene: new T3.Scene(),
        compMaterial: null,
        compMesh: null,
        compTexture: null
    });
    let {current: vars} = componentsVariables;

    // const [normalisedMousePos, setNormalisedMousePos] = useState<[number, number]>([0.5, 0.5]);
    const themeContext = useContext(ThemeContext);

    const handleMouseMove = useCallback((mouseEvent: MouseEvent & {layerX?: number, layerY?: number}) => {
        // if(mouseEvent.layerX && mouseEvent.layerY){
        //     setNormalisedMousePos([
        //         mouseEvent.layerX / vars.canvasRect.width,
        //         mouseEvent.layerY / vars.canvasRect.height
        //     ]);
        // }else{
        //     setNormalisedMousePos([
        //         (mouseEvent.clientX - vars.canvasRect.left) / vars.canvasRect.width, 
        //         (mouseEvent.clientY - vars.canvasRect.top) / vars.canvasRect.height
        //     ]);
        // }
    }, []);
    

    useEffect(() => {
        if(canvas){

            vars.compCamera.position.set(0, 0, 0.5);
            vars.compCamera.lookAt(0, 0, 0,);

            let i = 0;

            for ( let ix = 0; ix < SIZE; ix ++ ) {
                for ( let iy = 0; iy < SIZE; iy ++ ) {

                    let theta = Math.PI * 2 * Math.random();
                    let r = 0.5 + 0.5 * Math.random();

                    vars.data[ i + 0 ] = r * Math.cos(theta);
                    vars.data[ i + 1 ] = r * Math.sin(theta);
                    vars.data[ i + 2 ] = 1.0;
                    vars.data[ i + 3 ] = 1.0;

                    // velocity[ i ] = (Math.random() - 0.5) / 10;
                    // velocity[ i + 1 ] = (Math.random() - 0.5) / 10;
                    // velocity[ i + 2 ] =(Math.random() - 0.5) / 10;

                    i += 4;
                }
            }

            vars.compTexture = new T3.DataTexture(vars.data, SIZE, SIZE, T3.RGBAFormat, T3.FloatType);
            vars.compTexture.minFilter = T3.NearestFilter;
            vars.compTexture.magFilter = T3.NearestFilter;
            vars.compTexture.needsUpdate = true;

            vars.compMaterial = new T3.ShaderMaterial(
                {
                    uniforms: {
                        uPositions: {value: vars.compTexture},
                        time: {value: 0}
                    },
                    vertexShader: `
                        uniform float time;
                        varying vec2 vUv;
                        varying vec3 vPosition;

                        float PI = 3.141592653589793238;

                        void main(){
                            vUv = uv;
                            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                        }
                    `,
                    fragmentShader: `
                        uniform float time;
                        uniform sampler2D uPositions;
                        varying vec2 vUv;
                        varying vec3 vPosition; 

                        void main(){
                            vec4 pos = texture2D(uPositions, vUv);
                            float radius = length(pos.xy);
                            float angle = atan(pos.y, pos.x) - 0.1;

                            vec3 newPos = vec3(cos(angle), sin(angle), 0.) * radius;

                            pos.xy += (newPos.xy - pos.xy) * 0.1;

                            gl_FragColor = vec4(pos.xy, 1., 1.);
                        }
                    `
                }
            )

            vars.compMesh = new T3.Mesh(new T3.PlaneGeometry(2, 2), vars.compMaterial);
            vars.compScene.add(vars.compMesh);

            renderer.setRenderTarget(vars.fbo);
            renderer.render(vars.compScene, vars.compCamera);
            renderer.setRenderTarget(vars.fbo2);
            renderer.render(vars.compScene, vars.compCamera);
            
            vars.camera.position.set(0, 0, -2);
            vars.camera.lookAt(0, 0, 0);

            document.addEventListener('mousemove', handleMouseMove);

            vars.particleMaterial = new T3.ShaderMaterial({
                extensions:{
                    derivatives: true
                },
                side: T3.DoubleSide,
                uniforms:{
                    time: {value: vars.time},
                    uPositions: {value: vars.compTexture},
                    resolution: {value: new T3.Vector4()}
                },
                vertexShader: `
                    uniform float time;
                    uniform sampler2D uPositions;
                    varying vec2 vUv;
                    varying vec3 vPosition;

                    void main(){
                        vUv = uv;
                        vec4 pos = texture2D(uPositions, uv);
                        vec4 mvPosition = modelViewMatrix * vec4(pos.xyz, 1.);
                        gl_PointSize = 10. * (1. / - mvPosition.z);
                        gl_Position = projectionMatrix * mvPosition;
                    }

                `,
                fragmentShader: `
                    varying vec2 vUv;
                    uniform sampler2D uPositions;
                
                    void main(){
                        gl_FragColor = vec4(vUv, 0., 1.);
                    }
                `
            });

            let geometry = new T3.BufferGeometry();
            let positions = new Float32Array(vars.numParticles * 3);
            let uv = new Float32Array(vars.numParticles * 2);

            for ( let i = 0; i < SIZE; i ++ ) {
                for ( let j = 0; j < SIZE; j ++ ) {
                    let index = i + j * SIZE;

                    positions[index * 3 + 0] = Math.random();
                    positions[index * 3 + 1] = Math.random();
                    positions[index * 3 + 2] = 0;

                    uv[index * 2 + 0] = i / SIZE;
                    uv[index * 2 + 1] = j / SIZE;

                }
            }

            geometry.setAttribute('position', new T3.BufferAttribute(positions, 3));
            geometry.setAttribute('uv', new T3.BufferAttribute(uv, 2));

            let points = new T3.Points( geometry, vars.particleMaterial );
            vars.scene.add( points );
        }
    }, [canvas, handleMouseMove, themeContext, vars, renderer]);

    useImperativeHandle(ref, () => ({update}));

    const update = (deltaTime: number) : void => {

        if(vars.time + deltaTime > 300){
            vars.time = 0;
        }else{
            vars.time += deltaTime;
        }

        if(vars.compMaterial && vars.particleMaterial){
            vars.compMaterial.uniforms.time.value = vars.time;
            vars.particleMaterial.uniforms.time.value = vars.time;
        }


        if(vars.scene && vars.camera && vars.particleMaterial && vars.fbo && vars.fbo2 && vars.compMaterial){

            vars.compMaterial.uniforms.uPositions.value = vars.fbo2.texture;
            vars.particleMaterial.uniforms.uPositions.value = vars.fbo.texture;

            renderer.setRenderTarget(vars.fbo);
            renderer.render(vars.compScene, vars.compCamera);
            renderer.setRenderTarget(null);
            renderer.render(vars.scene, vars.camera);

            let temp = vars.fbo;
            vars.fbo = vars.fbo2;
            vars.fbo2 = temp;
        }
    }

    return null;
});

export default NavScene;
import * as T3 from "three";
import Player from "./Player";
import { TILE_EDGE_LENGTH } from "./Terrain";
import Tower from "./Tower";
import GameObject from "../GameObject";
import { PixelValue } from "engine/util/segmentLoader";
import PowerUp from "./PowerUp";
import { lightGreyMaterial } from "engine/util/materials";


const TOWER_MAX_HEIGHT = 10;

interface MapSlot {
    occupied: boolean,
    height: number
}

export default class Tile extends GameObject {
    scene: T3.Scene;
    mapData: MapSlot[][];
    height: number = 1;
    private activeTowers: Tower[] = [];
    private inactiveTowers: Tower[] = [];

    private activePowerUps: PowerUp[] = [];
    private inactivePowerUps: PowerUp[] = [];


    towerCount: number = 0;
    towerWidth: number = 0;
    towerDepth: number = 0;
    unoccupiedTowerCoords: [number, number][] = [];

    powerUpCount: number = 0;
    powerupCoords: [number, number][] = [];

    constructor(x: number, z: number, pattern: PixelValue[][], scene: T3.Scene) {

        super();
        const floorGeom = new T3.BoxGeometry(TILE_EDGE_LENGTH, 0.1, TILE_EDGE_LENGTH);
        this.mesh = new T3.Mesh(floorGeom, lightGreyMaterial);
        this.mesh.position.set(x, 0, z);
        this.mesh.receiveShadow = true;

        this.scene = scene;
        this.mapData = pattern.map((line) => line.map((slot) => { return {occupied: false, height: slot[1], powerUp: slot[2] > 0}}));
        this.towerWidth = TILE_EDGE_LENGTH/pattern.length;
        this.towerDepth = TILE_EDGE_LENGTH/pattern[0].length;
    }

    deactivateTowers(): void {
        while(this.activeTowers.length > 0){
            const activeTower = this.activeTowers.pop();
            if(activeTower){
                this.inactiveTowers.push(activeTower);
                this.scene.remove(activeTower.getMesh());
            }
        };
    }


    parsePattern(pattern: PixelValue[][]) {
        this.towerCount = 0;
        this.powerUpCount = 0;
        

        this.unoccupiedTowerCoords = [];
        this.powerupCoords = [];


        pattern.forEach((line, x) => {
            line.forEach((pixel, z) => {
                let r = pixel[0];
                let g = pixel[1];
                let b = pixel[2];

                if(g > 0){
                    this.towerCount++;
                    this.unoccupiedTowerCoords.push([x, z]);
                }
                if(b > 0){
                    this.powerUpCount++;
                    this.powerupCoords.push([x, z]);
                }

                this.towerCount += r? 1: 0;
                this.powerUpCount += pixel[2]? 1: 0;
            })
        })


        this.towerWidth = TILE_EDGE_LENGTH/pattern.length;
        this.towerDepth = TILE_EDGE_LENGTH/pattern[0].length;

        while(this.activeTowers.length + this.inactiveTowers.length < this.towerCount ){
            const tower = new Tower(this.towerWidth, 1, this.towerDepth);
            this.inactiveTowers.push(tower);
        }
        
        this.mapData = pattern.map((line) => line.map((mapSlot) => { return {occupied: false, height: mapSlot[1]}}));

        this.spawnPowerUps();
        while(this.unoccupiedTowerCoords.length){
            this.spawnTower();
        }
    }

    spawnPowerUps() {

        for(let i = 0; i < this.powerupCoords.length; i++){

            let coords = this.powerupCoords[i];

            const vec = new T3.Vector3(
                (coords[0] + 0.5) * this.towerWidth + this.mesh.position.x - TILE_EDGE_LENGTH/2, 
                0.06, 
                (coords[1] + 0.5) * this.towerDepth + this.mesh.position.z - TILE_EDGE_LENGTH/2
            );

            let powerUp = this.inactivePowerUps.pop()
            if(!powerUp) powerUp = new PowerUp(this.scene, vec); 
            
            this.activePowerUps.push(powerUp);
        }    
    }

    spawnTower() {
        if(this.unoccupiedTowerCoords.length) {
            let tower = this.inactiveTowers.pop()
            if(!tower) tower = new Tower();   
            
            const coordIndex = Math.floor(Math.random() * this.unoccupiedTowerCoords.length)
            let coords = this.unoccupiedTowerCoords[coordIndex];
            this.unoccupiedTowerCoords.splice(coordIndex, 1);
            
            tower.spawn(
                (coords[0] + 0.5) * this.towerWidth + this.mesh.position.x - TILE_EDGE_LENGTH/2, 
                0, 
                (coords[1] + 0.5) * this.towerDepth + this.mesh.position.z - TILE_EDGE_LENGTH/2
            );
            tower.getMesh().scale.x = this.towerWidth
            tower.getMesh().scale.y = (this.mapData[coords[0]][coords[1]].height/255) * TOWER_MAX_HEIGHT;
            tower.getMesh().scale.z = this.towerDepth
            this.mapData[coords[0]][coords[1]].occupied = true;
            this.activeTowers.push(tower);
            this.scene.add(tower.getMesh());
        }
    }

    update(addToEnergyCount: (x: number) => void, player: Player){

        const playerPos = player.getPosition();


        for(let i = 0; i < this.activeTowers.length; i++){
            const tower = this.activeTowers[i];
            if(tower.getPosition().z < playerPos.z - 3){
                this.inactiveTowers.push(this.activeTowers.splice(i, 1)[0])
                this.scene.remove(tower.getMesh());
            }
        }

        for(let i = 0; i < this.activePowerUps.length; i++){
            const powerUp = this.activePowerUps[i];
            powerUp.update(addToEnergyCount, player);
                
            if(powerUp.getPosition().z < playerPos.z - 3){
                this.activePowerUps.push(this.activePowerUps.splice(i, 1)[0])
                this.scene.remove(powerUp.getMesh());
            }
        }

    }

}
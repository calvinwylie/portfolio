import * as T3 from "three";
import Player from "./Player";
import { SEGMENT_LENGTH, SegmentName, segmentPatterns } from "engine/util/segmentLoader";

import Tile from "./Tile";

export const TILE_EDGE_LENGTH = 20;
export const TILES_PER_ROW = 5;
export const TILES_PER_COLUMN = 6;
export const MAX_DIST_TO_PLAYER = (TILES_PER_COLUMN - 0.5) * TILE_EDGE_LENGTH;

const segmentSequence: SegmentName[] = ["blank", "entrance"];


export default class Terrain {
    private scene: T3.Scene;
    private tiles: Tile[] = [];

    private targetPlayer: Player;


    constructor(scene: T3.Scene, player: Player) {

        this.targetPlayer = player;
        this.scene = scene; 

        // -- TILES
        for(let x = 0; x < TILES_PER_ROW; x++){
            for(let z = 0; z < TILES_PER_COLUMN; z++){
                this.tiles.push( new Tile(
                    x * TILE_EDGE_LENGTH,//(x - TILES_PER_ROW/ 2 + 0.5) * TILE_EDGE_LENGTH,
                    z * TILE_EDGE_LENGTH,
                    segmentPatterns['blank']![0][0],
                    scene
                ));

                scene.add(this.tiles[this.tiles.length - 1].getMesh());
                

                this.respawnTile(this.tiles[this.tiles.length - 1], this.tiles[this.tiles.length - 1].getPosition())
            }
        }

        this.targetPlayer.getPosition().x = ((segmentPatterns['entrance']!.length/2) - 0.5) * TILE_EDGE_LENGTH;
         
    }

    respawnTile(tile: Tile, tilePos: T3.Vector3){
        
        tile.deactivateTowers();

        
        const segmentIndex = Math.floor((tilePos.z / TILE_EDGE_LENGTH) / SEGMENT_LENGTH) % segmentSequence.length;
        const segmentName = segmentSequence[segmentIndex];
        const segment = segmentPatterns[segmentName];

        if(segment){
            const imageWidth = segment.length * TILE_EDGE_LENGTH;
            const imageHeight = segment[0].length * TILE_EDGE_LENGTH;

            let x = (tilePos.x % imageWidth) / TILE_EDGE_LENGTH;
            if(x < 0) {
                x += segment.length; 
            }
            let z = Math.abs((tilePos.z % imageHeight)/ TILE_EDGE_LENGTH);
            if(z < 0) {
                z += segment[0].length;
            }

            const selection = segment[x][z];
            tile.parsePattern(selection);
        }
    }

    update(addToEnergyCount: (x: number) => void){

        const playerPos = this.targetPlayer.getPosition()

        for(let tile of Object.values(this.tiles)){

            const tilePos = tile.getPosition()
            // Conveyor belt the tiles
            if(tilePos.x > playerPos.x + TILES_PER_ROW * TILE_EDGE_LENGTH/2){
                tilePos.x -= TILE_EDGE_LENGTH * TILES_PER_ROW;
                this.respawnTile(tile, tilePos);
            }else if(tilePos.x < playerPos.x - TILES_PER_ROW * TILE_EDGE_LENGTH/2){
                tilePos.x += TILE_EDGE_LENGTH * TILES_PER_ROW;
                this.respawnTile(tile, tilePos);
            }

            if(tilePos.z > playerPos.z + TILE_EDGE_LENGTH * (TILES_PER_COLUMN )){
                tilePos.z -= TILE_EDGE_LENGTH * TILES_PER_COLUMN;
                this.respawnTile(tile, tilePos);
            }else if(tilePos.z < playerPos.z - TILE_EDGE_LENGTH){
                tilePos.z += TILE_EDGE_LENGTH * (TILES_PER_COLUMN);
                this.respawnTile(tile, tilePos);
            }
        }

        for(let x = 0; x < TILES_PER_ROW; x++){
            for(let z = 0; z < TILES_PER_COLUMN; z++){
                this.tiles[x + z * TILES_PER_ROW].update(addToEnergyCount, this.targetPlayer);
            }
        }
    }

}
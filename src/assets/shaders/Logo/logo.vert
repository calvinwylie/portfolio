varying vec2 vUv;
varying vec3 vNormal;

void main() {
    vUv = uv;
    vNormal = normal;
    vec3 scale = vec3(1.0, 1.0, 1.5);
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position * scale, 1.0);
}
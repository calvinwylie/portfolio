import * as T3 from "three";
import {lightGreyMaterial} from 'engine/util/materials';
import GameObject from '../GameObject';

export default class Tower extends GameObject {

    private height: number;

    constructor(width: number = 1, height: number = 1, depth: number = 1) {
        super();

        const geom = new T3.BoxGeometry(width, height, depth);
        this.mesh = new T3.Mesh(geom, lightGreyMaterial);
        this.mesh.position.set(0,-1,-1);
        this.mesh.castShadow = true;

        this.height = height;
    }

    spawn(x: number, y: number, z: number){
        this.mesh.position.set(x, y + this.height/2, z);
    }
    
    update(){}

}
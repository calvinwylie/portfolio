varying vec2 vUv;
varying vec4 vNormal;
varying vec4 vPosition; 

void main() {
    vUv = uv;
    mat4 mvpMatrix = projectionMatrix * modelViewMatrix;
    vNormal = mvpMatrix * vec4(normal, 1.0);
    vPosition = mvpMatrix * vec4(position, 1.0);
    gl_Position = vPosition;
}
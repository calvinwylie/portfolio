import React, {FC, useContext} from 'react';
import {Link} from "react-router-dom";
import {mdiBookOpenOutline, mdiCircle, mdiClose, mdiGamepadVariantOutline, mdiGitlab, mdiHomeOutline, mdiLinkedin, mdiMenu} from "@mdi/js";
import Icon from "@mdi/react";
import cx from 'classnames';

import classes from "./Nav.module.css";
import { NavContext, ThemeContext } from 'components/App/App';
import Engine from 'engine/Engine';
import MenuScene from 'engine/scenes/MenuScenes/NavScene';

// const htmlEl = document.getElementsByTagName("html")[0] as HTMLElement;

interface pageMetadata {
    link: string,
    icon: string,
    name: string
}

const pages: pageMetadata[] = [{
    link: "/",
    icon: mdiHomeOutline,
    name: "Home"
},{
    link: "/story",
    icon: mdiBookOpenOutline,
    name: "About Me"
},{
    link: "/game_gallery",
    icon: mdiGamepadVariantOutline,
    name: "Game Gallery"
}]


const Nav: FC = (props) => {

    let themeContext = useContext(ThemeContext);
    let navContext = useContext(NavContext);
    // let [hoverIcon, setHoverIcon] = useState<typeof pages[number]['name'] | null>(null);

    return (
        <nav className={classes.Container}>
            <div
                data-tip={"Menu"}
                onClick={() => navContext.setState(!navContext.value)} 
                className={classes.MenuButton}
            >
                <Icon path={mdiMenu} size={2}/>
            </div>

            {navContext.value &&
                <div className={cx(classes.Menu)}>
                    <div
                        onClick={() => navContext.setState(!navContext.value)} 
                        className={classes.MenuButton}
                    >
                        <Icon path={mdiClose} size={2}/>
                    </div>

                    <div className={classes.MenuGrid}>
                        <div className={classes.Visualisation}>
                            <Engine 
                                clearColor={themeContext.value === 'dark'? 0x000000 : 0xFFFFFF}
                                renderFunction={(canvas, renderer, ref) => {
                                    return (
                                        <MenuScene 
                                            key={themeContext.value} 
                                            ref={ref} 
                                            canvas={canvas} 
                                            renderer={renderer}
                                        />
                                    )
                                }}
                            />
                        </div>
                        <div className={classes.Pages}>
                            <h2>Pages</h2>
                            <ul>
                                {pages.map((pageMetadata) => (
                                    <li 
                                        key={pageMetadata.link}
                                        // onMouseEnter={() => setHoverIcon(pageMetadata.icon)}
                                        // onMouseLeave={() => setHoverIcon(null)}
                                    >
                                        <Link to={pageMetadata.link} onClick={() => {navContext.setState(false)}}>
                                            <Icon path={mdiCircle}/> 
                                            <span>{pageMetadata.name}</span> 
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className={classes.Links}>
                            <h2>Links</h2>
                            <ul>
                                <li data-tip={"LinkedIn"} data-position={"right"}>
                                    <a href="https://www.linkedin.com/in/calvinwylie/" target={"_blank"} rel="noreferrer">
                                        <Icon path={mdiLinkedin} size={2}/>
                                        <span>LinkedIn</span>
                                    </a>
                                </li>
                                <li data-tip={"Gitlab"} data-position={"right"}>
                                    <a href="https://gitlab.com/calvinwylie" target={"_blank"}  rel="noreferrer">
                                        <Icon path={mdiGitlab} size={2}/>
                                        <span>GitLab</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            }

            
            {/* {htmlEl.dataset.theme &&
                <div 
                    className={classes.ModeSwitch}
                    data-tip={"Dark/Light Mode"} data-position={"right"}
                    onClick={() => {
                        themeContext.setState(themeContext.value === 'dark'? 'light': 'dark');
                        htmlEl.dataset["theme"] = themeContext.value === 'dark'? 'light': 'dark'; 
                    }}
                >
                    <Icon path={themeContext.value === 'dark'? mdiWhiteBalanceSunny: mdiWeatherNight}/>
                </div>
            } */}
        </nav>
    )
};

export default Nav;
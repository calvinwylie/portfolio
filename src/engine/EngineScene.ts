import * as T3 from 'three';

export interface EngineScene {
    toggleCamera: () => void;
    init: (canvas: HTMLCanvasElement) => void;
    update: (deltaTime: number) => void;
    render: (renderer: T3.WebGLRenderer) => void;
}

export interface SceneProps {
    canvas : HTMLCanvasElement;
    renderer: T3.WebGLRenderer;
    assets?: {[key: string]: any}
}
import React, {FC, ReactNode, useCallback, useEffect, useRef, useState} from 'react';
import * as T3 from 'three';
import Clock from './util/Clock';

export type InputType = 'toggleCamera'

export const CLOCK = new Clock();

type EngineProps = {
    clearColor: number
    renderFunction: (canvas: HTMLCanvasElement, renderer: T3.WebGLRenderer, ref: React.MutableRefObject<any>) => ReactNode;
}; 

const fps:              number = 60;
const frameDuration:    number = 1000 / fps;
let startTime:          number = Date.now();
let deltaTime:          number = 0;
let elapsed:            number = 0;


const Engine: FC<EngineProps> = ({clearColor, renderFunction}: EngineProps) => {

    const canvasRef = useRef<HTMLCanvasElement>(null);
    const sceneRef = useRef();
    const [renderer, setRenderer] = useState<T3.WebGLRenderer | null>(null);

    const update = useCallback((): void => {

        const current = Date.now();
        elapsed = current - startTime;
        startTime = current;

        deltaTime += elapsed;

        CLOCK.update(deltaTime);

        if(deltaTime > frameDuration && sceneRef.current){
            //@ts-ignore
            sceneRef.current.update(deltaTime/1000);
            deltaTime = 0;
        }
        requestAnimationFrame(update);
    }, []);

    useEffect(() => {

        if(canvasRef.current){
            let webGLRenderer = new T3.WebGLRenderer({
                canvas: canvasRef.current,
                premultipliedAlpha: true,
                antialias: true
            });
            setRenderer(webGLRenderer);
        
            webGLRenderer.setClearColor(clearColor);
            webGLRenderer.setSize(canvasRef.current.offsetWidth, canvasRef.current.offsetHeight);

            update();
        }

    }, [update, clearColor])
    

    return (
        <>
            { canvasRef.current && renderer && 
                renderFunction(canvasRef.current, renderer, sceneRef)
            }
            <canvas ref={canvasRef}></canvas>
        </>
    )
}

export default Engine;

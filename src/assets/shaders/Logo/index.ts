import fragment from './logo.frag';
import vertex from './logo.vert';

const logoShaders = {
    fragment,
    vertex
}

export default logoShaders;
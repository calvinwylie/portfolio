varying float vLength;
varying vec3 vPos;

void main() {
    float normalisedAboveSeaLevel = (vLength - 1.0) * 20.0;

    vec4 color; 

    if(normalisedAboveSeaLevel <= 0.01){
        color = mix(vec4(0.0, 0.0, 0.7, 1.0), vec4(0.0, 0.5, 1.0, 1.0), smoothstep(0.0, 0.00003, normalisedAboveSeaLevel));
        color = mix(color, vec4(0.2, 0.8, 0.8, 1.0), smoothstep(0.00003, 0.01, normalisedAboveSeaLevel));
    }else{
        color = vec4(0.2, 0.8, 0.8, 1.0);
        color = mix(color, vec4(0.0, 0.8, 0.2, 1.0), smoothstep(0.01, 0.02, normalisedAboveSeaLevel));
        color = mix(color, vec4(0.0, 0.6, 0.2, 1.0), smoothstep(0.02, 0.1, normalisedAboveSeaLevel));
        color = mix(color, vec4(0.0, 0.3, 0.1, 1.0), smoothstep(0.1, 0.7, normalisedAboveSeaLevel));
        color = mix(color, vec4(1.0, 1.0, 1.0, 1.0), smoothstep(0.7, 1.0, normalisedAboveSeaLevel+abs(vPos.y*vPos.y)));
    }

    gl_FragColor = color;
}
             
import * as T3 from 'three';

import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import ShaderLoader from 'components/_Utils/ShaderLoader';

const textureLoader = new T3.TextureLoader();
const shaderLoader = new ShaderLoader();
const objLoader = new OBJLoader();

export {
    textureLoader,
    shaderLoader,
    objLoader
}
import {Position} from 'geojson';

export const StoryTags = [
    "career", 
    "education",
    "life event" 
] as const;

export type StoryTag = typeof StoryTags[number]

export interface StoryStep {
    tags: Array<StoryTag>,
    title: string,
    establishment?: string,
    startdate: string,
    enddate?: string,
    position: Position,
    location: string,
    path?: string
}


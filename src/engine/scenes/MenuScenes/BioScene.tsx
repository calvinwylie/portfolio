import { SceneProps } from 'engine/EngineScene';
import * as T3 from 'three';
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import SpotEarth from 'engine/gameobjects/Menu/SpotEarth';


interface BioSceneProps extends SceneProps {
    assets: {
        earthSpecular: T3.Texture,
        cityEmissive: T3.Texture,
        cityShaders: [string, string]
    };
    latLong: [number, number];
}

let dragging = false;

const BioScene = forwardRef(({canvas, renderer, assets, latLong}: BioSceneProps, ref) => {

    let [frameBuffer, setFrameBuffer] = useState<T3.WebGLRenderTarget | null>(null);
    let [frameBuffer2, setFrameBuffer2] = useState<T3.WebGLRenderTarget | null>(null);
    let [bufferScene, setBufferScene] = useState<T3.Scene | null>(null);
    let [scene, setScene] = useState<T3.Scene | null>(null);
    let [camera, setCamera] = useState<T3.Camera | null>(null);
    let [orthoCamera, setOrthoCamera] = useState<T3.Camera | null>(null);
    let [earth, setEarth] = useState<SpotEarth | null>(null); 
    let [flipFlop, setFlipFlop] = useState<boolean>(false);
    let [particles, setParticles] = useState<T3.Points | null>();

    let handleMouseMove = useCallback((event) => {
        if(dragging && earth){
            earth.rotate(event.movementX, event.movementY);
        }
    }, [earth])

    let handleMouseDown = useCallback(() => {
        dragging = true;
    }, [])

    let handleMouseUp = useCallback(() => {
        dragging = false;
    }, [])


    if(canvas){
        canvas.removeEventListener("mousemove", handleMouseMove);
        canvas.removeEventListener("mousedown", handleMouseDown);
        canvas.removeEventListener("mouseup", handleMouseUp);

        canvas.addEventListener("mousemove", handleMouseMove);
        canvas.addEventListener("mousedown", handleMouseDown);
        canvas.addEventListener("mouseup", handleMouseUp);
    }

    useEffect(() => {
 
        if(canvas){

            let width = canvas.offsetWidth;
            let height = canvas.offsetHeight;

            const scene_init = new T3.Scene();
            setScene(scene_init);

            const bufferScene_init = new T3.Scene();
            setBufferScene(bufferScene_init);

            const camera_init = new T3.PerspectiveCamera(50, canvas.width/canvas.height, 0.1, 200);
            setCamera(camera_init);

            setFrameBuffer(new T3.WebGLRenderTarget(width, height));
            setFrameBuffer2(new T3.WebGLRenderTarget(width, height));

            const orthoCamera_init = new T3.OrthographicCamera(
                -canvas.offsetWidth/2,
                canvas.offsetWidth/2,
                canvas.offsetHeight/2,
                -canvas.offsetHeight/2, 
                0.1, 
                1000
            );
            setOrthoCamera(orthoCamera_init);

            setEarth(new SpotEarth(
                bufferScene_init,
                assets.earthSpecular
            ));
            
            let separation = 9;

            let numParticlesX = Math.floor(width/separation) + 1;
            let numParticlesY = Math.floor(height/separation) + 1;

            let numParticles = numParticlesX * numParticlesY;

            let positions = new Float32Array(numParticles * 3);

            let i = 0;

            for ( let ix = 0; ix < numParticlesX; ix ++ ) {
                for ( let iy = 0; iy < numParticlesY; iy ++ ) {

                    positions[ i ] = ix * separation - ( ( numParticlesX * separation ) / 2 ); // x
                    positions[ i + 1 ] = iy * separation - ( ( numParticlesY * separation ) / 2 ); // y
                    positions[ i + 2 ] = -1;

                    i += 3;
                }
            }

            let geometry = new T3.BufferGeometry();
            geometry.setAttribute('position', new T3.BufferAttribute(positions, 3));

            let uniforms = {
                texture1: {type: "t", value: assets.earthSpecular}
            }


            const bgPlaneMaterial = new T3.ShaderMaterial( {
                uniforms,
                vertexShader: `
                    uniform sampler2D texture1;
                    varying float vScale;
                    varying vec4 vColor;

                    void main() {

                        vec4 fragCoord = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                        fragCoord.xyz /= fragCoord.w;
                        fragCoord.w = 1.0 / fragCoord.w;
                        fragCoord.xyz *= vec3(0.5); 
                        fragCoord.xyz += vec3(0.5); 

                        vec4 color = texture2D(texture1, fragCoord.xy);
                        vColor = color;
                        vScale = ${separation - 1}. * color.b;
                        gl_PointSize = vScale;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                    }
                `,
                fragmentShader: `
                    uniform sampler2D texture1;
                    varying float vScale;
                    varying vec4 vColor;

                    vec4 white = vec4(1., 1., 1., 1.);

                    void main() {
                        if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;

                        if(vColor.r >= vColor.g - 0.1 && vColor.r <= vColor.g + 0.1){
                            gl_FragColor = vColor;
                        }else{
                            gl_FragColor = vec4(0.9, 0.6, 0.0, 1.0);
                        }
                    }
                `
            });

            let particles = new T3.Points( geometry, bgPlaneMaterial );
            setParticles(particles);
            scene_init.add( particles );

            camera_init.position.set(0, 0, -5);
            camera_init.lookAt(0, 0, 0);

            const directionalLight = new T3.DirectionalLight(0xffffff, 1);
            directionalLight.position.set(-5, 5, -15);
            directionalLight.castShadow = true;
            directionalLight.shadow.mapSize.width = 2048; // default
            directionalLight.shadow.mapSize.height = 2048; // default
            bufferScene_init.add(directionalLight);

        }

    }, [
        canvas,
        assets.earthSpecular
    ]);

    useImperativeHandle(ref, () => ({update}));


    const update = (deltaTime: number) : void => {
        if(!scene || !bufferScene || !camera || !orthoCamera || !particles || !frameBuffer || !frameBuffer2) return;

        let activeFrameBuffer = flipFlop? frameBuffer: frameBuffer2;

        renderer.setRenderTarget(activeFrameBuffer);
        renderer.render(bufferScene, camera);

        (particles.material as T3.ShaderMaterial).uniforms.texture1.value = activeFrameBuffer.texture;
        (particles.material as T3.ShaderMaterial).needsUpdate = true;

        renderer.setRenderTarget(null);
        renderer.render(scene, orthoCamera);

        setFlipFlop(!flipFlop)

        if(earth){
            earth.setTargetRotation(latLong);
            earth.update(deltaTime);
        }

    };

    return null;
});

export default BioScene;
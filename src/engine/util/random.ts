export function mulberry32(a: number) {
    return function() {
      var t = a += 0x6D2B79F5;
      t = Math.imul((t ^ t) >>> 15, t | 1);
      t ^= t + Math.imul((t ^ t) >>> 7, t | 61);
      return (((t ^ t) >>> 14) >>> 0) / 4294967296;
    }
}

const SEED = 1690327399070; // Date.now();
console.log("seed: " + SEED);

export const rand = mulberry32(SEED)

export function getCSSVar(variableName: string){
    return getComputedStyle(document.documentElement).getPropertyValue(`--${variableName}`); 
}
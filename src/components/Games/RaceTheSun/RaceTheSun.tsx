
//@ts-nocheck

import React from 'react';
import Engine from 'engine/Engine';

import classes from './RaceTheSun.module.css';
import * as T3 from 'three';
import cx from 'classnames';
import RTC_MainScene from 'engine/scenes/ChaseTheSun/MainScene';


export interface HUDData{
    triCount: number,
    location: T3.Vector3,
    score: number,
    multiplier: number,
    showDebugInfo: boolean
}

type RaceTheSunState = HUDData

class RaceTheSun extends React.Component<RaceTheSunState> {


    state: RaceTheSunState = {
        triCount:  0,
        location: new T3.Vector3(0,0,0),
        score: 0,
        multiplier: 1,
        showDebugInfo: false
    }

    // engine: Engine | null = null;
    static CONTAINER_COUNT = 5;

    componentDidMount(): void {
        // this.engine = new Engine(new RTC_MainScene(this.updateDisplay, this.toggleDebugInfo));
        // this.engine.update();
    }

    updateDisplay = (displayUpdate: Partial<HUDData>) : void => {
        this.setState(displayUpdate);
    }

    toggleDebugInfo = (forceChoice?: boolean) => {
        this.setState({showDebugInfo: forceChoice ?? !this.state.showDebugInfo}); 
    }

    renderCrystalContainer(currentFill: number) {
        
        const containers : React.ReactNode[] = []
        

        for(let i = 0; i < RaceTheSun.CONTAINER_COUNT; i++){
            containers.push(<div className={cx(classes.Crystal, {[classes.Filled]: i < currentFill})}></div>);
        }

        return (
            <div className={classes.CrystalContainer}>
                {containers}
            </div>
        )
    }

    render(): React.ReactNode {

        const currentFill = this.state.triCount % (RaceTheSun.CONTAINER_COUNT + 1);

        return (
            <>
                <Engine scene={new RTC_MainScene(this.updateDisplay, this.toggleDebugInfo)}>
                    <div className={classes.Overlay}>
                        <div className={classes.TopLeft}> 
                            <div className={classes.Clip}>
                                <p className={classes.Score}>{Math.floor(this.state.score)}</p>
                                <p className={classes.Multiplier}>x{Math.floor(this.state.multiplier)}</p>
                            </div>
                            
                            {this.renderCrystalContainer(currentFill)}
                        </div>


                        {this.state.showDebugInfo && 
                            <>
                                <p>Energy: {this.state.triCount}</p>
                                <p>x: {this.state.location.x},</p>aa
                                <p>y: {this.state.location.y},</p>
                                <p>z: {this.state.location.z}</p>
                                {/* <p onClick={() => this.engine?.externalInput("toggleCamera")}>toggleCamera</p> */}
                            </>
                        }
                    </div>
                </Engine>
            </>
        )
    }
}

export default RaceTheSun;
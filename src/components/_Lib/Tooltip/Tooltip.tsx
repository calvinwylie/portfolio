import React, {FC, useEffect, useState} from 'react';
import cx from 'classnames';

import classes from './Tooltip.module.css';

const TOOLTIP_PADDING = 10;
const LINEHEIGHT_MULTIPLIER = 1.2;
const TOOLTIP_ARROW_MARGIN = 10;


const Tooltip: FC = (props) => {

    const [x, setX] = useState(0);
    const [y, setY] = useState(0);
    const [tip, setTip] = useState('');
    const [position, setPosition] = useState('');

    const getStringLength = (string: string) => {
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext("2d")!;
        ctx.font = `${getFontSize()}px ${getFontFamilty().split(',')[0]}`;
        return ctx.measureText(string).width;
    };

    const getFontFamilty = () => {
        const computedStyle = getComputedStyle(document.documentElement);
        return computedStyle.fontFamily;
    };

    const getFontSize = () => {
        const computedStyle = getComputedStyle(document.documentElement);
        const rem = parseFloat(computedStyle.fontSize);
        const fontSize = parseFloat(computedStyle.getPropertyValue('--font-size-md'));
        return rem * fontSize;
    };

    const getTip = (event: MouseEvent) => {
        const target = event.target as HTMLElement;
        if(target.closest){
            let elemWithTip = target.closest("[data-tip]") as HTMLElement;
            if(elemWithTip){
                const position = elemWithTip.dataset?.['position'] ?? 'right';
                const {left, right, top, bottom} = elemWithTip.getBoundingClientRect();
                const tip = elemWithTip.dataset['tip']!;
                const tipElementWidth = getStringLength(tip) + TOOLTIP_PADDING;
                const tipElementHeight = getFontSize() * LINEHEIGHT_MULTIPLIER + TOOLTIP_PADDING;
                switch(position){
                    case 'top':
                        setX(right - (elemWithTip.offsetWidth / 2) - (tipElementWidth / 2));
                        setY(top - tipElementHeight - TOOLTIP_ARROW_MARGIN);
                        break;
                    case 'bottom':
                        setX(right - (elemWithTip.offsetWidth / 2) - (tipElementWidth / 2));
                        setY(bottom + TOOLTIP_ARROW_MARGIN);
                        break;
                    case 'right':
                        setX(right + TOOLTIP_ARROW_MARGIN);
                        setY(top + (elemWithTip.offsetHeight / 2) - (tipElementHeight / 2));
                        break;
                    case 'left':
                        setX(left - tipElementWidth - TOOLTIP_ARROW_MARGIN);
                        setY(top + (elemWithTip.offsetHeight / 2) - (tipElementHeight / 2));
                        break;
                }
                setTip(tip);
                setPosition(position);
            }else{
                setTip('');
            }
        }
    };

    useEffect(() => {
        document.addEventListener('mouseover', getTip);
        return () => {
            document.removeEventListener('mouseover', getTip)
        };
    });

    return tip?
            <div style={{left: x, top: y}} className={cx(classes.Container, classes[position])} id={'tip'}>
                {tip}
            </div>
        : null;
};

export default Tooltip;
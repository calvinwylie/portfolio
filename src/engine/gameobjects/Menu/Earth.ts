import * as T3 from "three";

import GameObject from "../GameObject";

const EARTH_SIZE_MULTIPLIER = 2;

export default class Earth extends GameObject {

    earth?: T3.Object3D;
    cities?: T3.Object3D;
    atmosphere?: T3.Object3D;

    constructor(
        scene: T3.Scene,
        earthDiffuse: T3.Texture,
        earthBump: T3.Texture,
        earthSpecular: T3.Texture,
        cityEmissive: T3.Texture,
        cityShaders: [string, string],
        atmosShaders: [string, string]
    ) {
        super();

        const uniforms = T3.UniformsUtils.merge([
            T3.UniformsLib['lights'],
            {
                texture1: { type: "t", value: cityEmissive }
            }
        ]);
        const earthMaterial = new T3.MeshPhongMaterial({
            map: earthDiffuse,
            bumpMap: earthBump,
            bumpScale: 0.01,
            specularMap: earthSpecular,
            specular: new T3.Color(0xaaaaaa),
            shininess: 30
        });
        const earthGeom = new T3.SphereGeometry(EARTH_SIZE_MULTIPLIER, 50, 50);
        const earthMesh = new T3.Mesh(earthGeom, earthMaterial);
        earthMesh.castShadow = true;
        this.earth = earthMesh;

        const cityMaterial = new T3.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: cityShaders[0],
            fragmentShader: cityShaders[1],
            depthTest: false,
            lights: true,
            transparent: true
        });
        const citiesGeom = new T3.SphereGeometry(EARTH_SIZE_MULTIPLIER);
        const citiesMesh = new T3.Mesh(citiesGeom, cityMaterial);
        this.cities = citiesMesh;

        let atmosMaterial = new T3.ShaderMaterial({
            uniforms: T3.UniformsLib['lights'],
            vertexShader: atmosShaders[0],
            fragmentShader: atmosShaders[1],
            transparent: true,
            lights: true,
            blending: T3.AdditiveBlending
        });
        const atmosGeom = new T3.SphereGeometry(EARTH_SIZE_MULTIPLIER * 1.1, 40, 40);
        const atmosMesh = new T3.Mesh(atmosGeom, atmosMaterial);

        scene.add(earthMesh);
        scene.add(citiesMesh);
        scene.add(atmosMesh);
    }

    getPosition(): T3.Vector3 {
        return this.earth?.position ?? new T3.Vector3(5);
    }

    setRotation(normalisedMousePos: [number, number]) {
        if(this.earth && this.cities){
            this.earth.rotation.x = -(Math.PI * (normalisedMousePos[1] - 0.5));
            this.earth.rotation.y = Math.PI * 2 * (normalisedMousePos[0] - 0.5);

            this.cities.rotation.x = -(Math.PI * (normalisedMousePos[1] - 0.5));
            this.cities.rotation.y = Math.PI * 2 * (normalisedMousePos[0] - 0.5);
        }
    }

    async init(scene: T3.Scene, vec: T3.Vector3) {

 
    }

    update(){}

}
import React, {FC, useContext} from 'react';

import Engine from 'engine/Engine';
import HomeScene from 'engine/scenes/MenuScenes/HomeScene';
import { NavContext, ThemeContext } from 'components/App/App';
import useLoadAssets from 'components/_Utils/UseLoadAssets';
import { objLoader, textureLoader, shaderLoader } from 'engine/util/Loaders';

import LogoModel from 'assets/objects/Menu/logo.obj';
import LogoShaders from 'assets/shaders/Logo';

import LogoOutlineTexture from 'assets/textures/Menu/logo.png';
import JWSTTexture from 'assets/textures/Menu/jwst_full.png';

const HomePage: FC = () => {

    const themeContext = useContext(ThemeContext); 
    const navContext = useContext(NavContext);

    const data = useLoadAssets([
        objLoader.loadAsync(LogoModel),
        textureLoader.loadAsync(LogoOutlineTexture),
        textureLoader.loadAsync(JWSTTexture),
        shaderLoader.loadAsync(LogoShaders.vertex),
        shaderLoader.loadAsync(LogoShaders.fragment),
        new Promise<void>((r, rj) => {
            setTimeout(() => r(), 0);
        })
    ]);

    return (
        <>
            {data && !navContext.value &&
                <Engine 
                    clearColor={themeContext.value === 'dark'? 0x000000 : 0xFFFFFF}
                    renderFunction={(canvas, renderer, ref) => {
                        return (
                            <HomeScene 
                                key={themeContext.value} 
                                ref={ref} 
                                canvas={canvas} 
                                renderer={renderer} 
                                assets={{
                                    logoModel: data[0],
                                    logoTexture: data[1],
                                    backgroundTexture: data[2],
                                    logoShaders: [data[3], data[4]]
                                }}
                            />
                        )
                    }}
                />
            }
            {/* <div className={classes.Container}>
                <div className={classes.UIOverlay}>
                    <div className={classes.Text}>
                        <p>Calvin&nbsp;Wylie</p>
                        <p>web&nbsp;development</p>
                    </div>
                </div>
            </div> */}
        </>
    )
};

export default HomePage;
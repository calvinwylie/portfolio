import React, {FC} from 'react';
import Icon from "@mdi/react";
import {mdiLinkedin, mdiGitlab} from "@mdi/js";

import classes from "components/Partials/Socials/Socials.module.css";

const Socials: FC = (props) => {
    return (
        <nav className={classes.Navigation}>
            <ul>
                <li data-tip={"LinkedIn"} data-position={"left"}>
                    <a href="https://www.linkedin.com/in/calvinwylie/" target={"_blank"} rel="noreferrer">
                        <Icon path={mdiLinkedin} size={2}/>
                    </a>
                </li>
                <li data-tip={"Gitlab"} data-position={"left"}>
                    <a href="https://gitlab.com/calvinwylie" target={"_blank"}  rel="noreferrer">
                        <Icon path={mdiGitlab} size={2}/>
                    </a>
                </li>
            </ul>
        </nav>
    )
};

export default Socials;
import * as T3 from "three";

import GameObject from "../GameObject"
import Player from "./Player";


export default class Skybox extends GameObject {

    uniformData = {}

    constructor(scene: T3.Scene) {
        super();
        const skyboxMaterial = new T3.ShaderMaterial ({
            uniforms: this.uniformData,
            side: T3.DoubleSide,
            vertexShader: `
            varying vec2 vUv;

            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
             `,
            fragmentShader: `

            varying vec2 vUv;

            vec3 colorSky = vec3(0.0, 0.0, 0.0);
            vec3 colorHorizon = vec3(0.8, 0.8, 0.8);

            void main() {
                vec3 color = mix(colorHorizon, colorSky, abs(vUv[1] - 0.5) * 2.0);
                gl_FragColor = vec4(color, 1.0);
            }
             `
        })


        const geom = new T3.BoxGeometry(250, 175, 200);
        this.mesh = new T3.Mesh(geom, skyboxMaterial);
        this.mesh.renderOrder = -1;
        this.mesh.position.x = 20;
        scene.add(this.mesh);
    }

    update(deltaTime: number, player: Player){
        this.mesh.position.x = player.getPosition().x;
        this.mesh.position.y = player.getPosition().y;
        this.mesh.position.z = player.getPosition().z;
    }
}
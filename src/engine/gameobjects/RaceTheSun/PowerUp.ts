import * as T3 from "three";
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import Player from "./Player";
import GameObject from "../GameObject";
import { CLOCK } from "engine/Engine";


export default class PowerUp extends GameObject {

    uniformData = {
        u_time: {
            type: 'f',
            value: 0
        }
    }
    object?: T3.Object3D;

    constructor(scene: T3.Scene, vec: T3.Vector3) {
        super();
        this.init(scene, vec);
    }

    getPosition(): T3.Vector3 {
        return this.object?.position ?? new T3.Vector3(5);
    }

    async init(scene: T3.Scene, vec: T3.Vector3) {

        const powerUpMaterial = new T3.ShaderMaterial ({
            // wireframe: true,
            uniforms: this.uniformData,
            transparent: true,
            side: T3.DoubleSide,
            blendSrcAlpha: 1,
            depthWrite: false,
            // blending: T3.AdditiveBlending,
            vertexShader: `
            attribute vec3 a_barycentric;L

            uniform float u_time;

            varying vec3 vbc;
            varying vec2 vUv;

            void main() {
                // vec4 result;

                vbc = a_barycentric;
                vUv = uv;

                // result =  vec4(position.x, sin(u_time) * 0.2 + position.y, position.z, 1.0);

                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
             `,
            fragmentShader: `

            varying vec3 vbc;
            varying vec2 vUv;

            vec3 colorA = vec3(0.54, 0.83, 1.0);
            vec3 colorB = vec3(0.0, 0.0, 0.8);

            void main() {
                float width = 0.2;
                float alphMul = 1.0;
                
                float alpha = 0.0;
                if(vbc.x < width){
                    alpha = max(alphMul *(1.0 - vbc.x / width), alpha);
                } 
                if (vbc.y < width){
                    alpha = max(alphMul *(1.0 - vbc.y / width), alpha);
                } 
                if (vbc.z < width){
                    alpha = max(alphMul *(1.0 - vbc.z / width), alpha);
                }

                vec3 color = mix(colorB, colorA, alpha);

                gl_FragColor = vec4(color, alpha * alpha * alpha);
            }
             `
        })

        const loader = new OBJLoader();
        let object = await loader.loadAsync('/objects/tri.obj');

        const self = this;
        
        object.traverse( function ( child ) {
            if ( child instanceof T3.Mesh ) {
                child.material = powerUpMaterial;
                const pos = child.geometry.attributes.position;
                const bary = self.calculateBarycentric(pos.array.length);
                self.mesh = child;
                (child.geometry as T3.BufferGeometry).setAttribute('a_barycentric', new T3.BufferAttribute(bary, 3));
            }
            scene.add(object)
        });   
        this.object = object; 
        this.object.position.x = vec.x;    
        this.object.position.y = vec.y;  
        this.object.position.z = vec.z; 
    }

    calculateBarycentric (length: number) {
        const n = length / 6;
        const barycentric = [];
        for (let i = 0; i < n; i++) barycentric.push(1, 0, 0, 0, 1, 0, 0, 0, 1);
        return new Float32Array(barycentric);
    }
      

    spawn(position: number[]){
        if(this.object){
            this.object.position.set(position[0], position[1], position[2]);
        }
    }
    
    update(handleEnergyCount: (x: number) => void, player: Player){

        this.uniformData.u_time.value = CLOCK.elapsedTime

        
        if(this.object){

            const sqrDistanceToPlayer = new T3.Vector3(this.object.position.x, 0, this.object.position.z).sub(player.getPosition()).lengthSq();

            let speed = this.object.position.y


            if(this.object.position.y - this.object.scale.y/2 > 0){
                this.object.position.y -= speed / 10;
            }
    
            if (sqrDistanceToPlayer < 2 && this.object.visible){
                handleEnergyCount(1);
                this.object.visible = false;
            }
    
            this.object.rotateY(Math.PI/180);
    
        }


    }

}
Leading the tech department was a huge learning experience for me. Moving further from
the day to day development and moving towards a more strategic position. Having to manage
6 staff as well as multiple verticals of software development. 

## Accomplishments

- Transitioned company cloud architecture from Digital Ocean to Microsoft Azure. 
- Release the company's first client facing API.  

## Responsibilities

- Code review and release of back and front end software. 
- Architecture Design and implmentation of cloud systems, databases and data storage.   
- Ideation and tech input into product development.
- Mentorship of junior and new tech members. 
- Internal tooling for Intelligence Team. 
- Repo and CICD management. 
- Managing the budget and accounts for the tech department. 


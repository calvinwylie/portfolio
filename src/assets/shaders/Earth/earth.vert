varying float vLength;
varying vec3 vPos;

void main() {
    vLength = length(position);
    vPos = position;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
const dir_path = "/patterns"

export const segmentNames = [
    "blank",
    "entrance",
    "test"
] as const;

export const SEGMENT_LENGTH = 6;

export type SegmentName = typeof segmentNames[number];
export type PixelValue = [number, number, number];
export type Pattern = PixelValue[][][][];

export let segmentPatterns:  {[key in SegmentName]?: Pattern} = {};


const blobToBase64 = (blob: Blob) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise(resolve => {
      reader.onloadend = () => {
        resolve(reader.result);
      };
    });
  };

export async function loadPatterns (callback: () => any) {
    for(let segmentName of segmentNames) {
        let fileRes = await fetch(`${dir_path}/${segmentName}.bmp`);
        let blob = await fileRes.blob()

        //@ts-ignore: FIX THIS
        let base64: string = await blobToBase64(blob)
            
        const image = new Image();
        image.onload = () => {
            
            var canvas = document.createElement('canvas');
            canvas.width = image.width;
            canvas.height = image.height;

            var context = canvas.getContext('2d');
            context?.drawImage(image, 0, 0);

            var imageData = context!.getImageData(0, 0, canvas.width, canvas.height);
            
            const SEGMENT_SIZE = 20;

            const segment: Pattern = []


            for(let segmentX = 0; segmentX  < image.width/SEGMENT_SIZE; segmentX++){
                segment[segmentX] = [];

                for(let segmentY = 0; segmentY  < image.height/SEGMENT_SIZE; segmentY++){
                    segment[segmentX][segmentY] = [];

                    for(let x = 0; x < SEGMENT_SIZE; x++){
                        segment[segmentX][segmentY].push([])
                        for(let y = 0; y < SEGMENT_SIZE; y++){

                            const currX = segmentX * SEGMENT_SIZE + x;
                            const currY = segmentY * SEGMENT_SIZE + y;

                            var index = currY * (imageData.width * 4) + currX * 4;

                            var r = imageData.data[index];
                            var g = imageData.data[index + 1];
                            var b = imageData.data[index + 2];
        
                            segment[segmentX][segmentY][x].push([r, g, b]);
                        }
                    }
                }
            }

            segmentPatterns[segmentName] = segment;

            if(Object.keys(segmentPatterns).length === segmentNames.length){
                callback();
            }
        };
        image.src = base64
            
    }
    
}


import * as T3 from "three";
import Player from "./Player";

export default class Sun {

    mesh: T3.Mesh;
    light: T3.PointLight;
    playerOffset: T3.Vector3;

    height: number = 1

    constructor() {
        
        const geom = new T3.CircleGeometry(2.5, 20);
        const material = new T3.MeshBasicMaterial()
        material.fog = false;

        this.mesh = new T3.Mesh(geom, material);
        this.light = new T3.PointLight(0xFFFFFF, 2);
        // this.light.castShadow = true;

        this.light.shadow.mapSize.width = 2048; // defaultw
        this.light.shadow.mapSize.height = 2048; // default
        this.light.shadow.camera.near = 0.5; // default
        this.light.shadow.camera.far = 200; // default

        // this.light.shadow.camera.fov = 5;
        
        
        this.light.shadow.camera.position.set(this.light.position.x, this.light.position.y, this.light.position.z );
        this.playerOffset = new T3.Vector3(0, 20, 90);

        this.mesh.rotateX(Math.PI)
    }

    raiseSun(){
        // this.playerOffset.y += 1;
    }

    lowerSun(){
        // this.playerOffset.y -= 0.0;
    }


    update(deltaTime: number, player: Player){

        const playerPosition = player.getPosition();

        this.mesh.position.x = playerPosition.x + this.playerOffset.x;
        this.mesh.position.y = playerPosition.y + this.playerOffset.y;
        this.mesh.position.z = playerPosition.z + this.playerOffset.z;
    
        this.light.position.x = playerPosition.x + this.playerOffset.x;
        this.light.position.y = playerPosition.y + this.playerOffset.y;
        this.light.position.z = playerPosition.z + this.playerOffset.z - 5;

        this.light.shadow.camera.lookAt(playerPosition);

    }

}
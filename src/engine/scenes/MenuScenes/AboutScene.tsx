import { SceneProps } from 'engine/EngineScene';
import Earth from 'engine/gameobjects/Menu/Earth';
import Skybox from 'engine/gameobjects/Menu/Skybox';
import * as T3 from 'three';
import { forwardRef, useCallback, useEffect, useRef, useState, useImperativeHandle } from 'react';


interface AboutSceneProps extends SceneProps {
    assets: {
        earthDiffuse: T3.Texture,
        earthBump: T3.Texture,
        earthSpecular: T3.Texture,
        cityEmissive: T3.Texture,
        cityShaders: [string, string],
        atmosShaders: [string, string]
    }
}

const AboutScene = forwardRef(({canvas, renderer, assets}: AboutSceneProps, ref) => {

    let [scene, setScene] = useState<T3.Scene | null>(null);
    let [camera, setCamera] = useState<T3.Camera | null>(null);
    let [earth, setEarth] = useState<Earth | null>(null); 
    
    const [normalisedMousePos, setNormalisedMousePos] = useState<[number, number]>([0.5, 0.5]);
    
    let canvasRect = useRef(canvas.getBoundingClientRect());
    
    const handleMouseMove = useCallback((mouseEvent: MouseEvent & {layerX?: number, layerY?: number}) => {
        if(mouseEvent.layerX && mouseEvent.layerY){
            setNormalisedMousePos([
                mouseEvent.layerX/canvasRect.current.width,
                mouseEvent.layerY/canvasRect.current.height
            ]);
        }else{
            setNormalisedMousePos([
                (mouseEvent.clientX - canvasRect.current.left) / canvasRect.current.width, 
                (mouseEvent.clientY - canvasRect.current.top) / canvasRect.current.height
            ]);
        }
    }, []);


    useEffect(() => {

        const newScene = new T3.Scene();
        const newCamera = new T3.PerspectiveCamera(50, canvas.width/canvas.height, 0.1, 200);

        setScene(newScene);
        setCamera(newCamera);
        setEarth(new Earth(
            newScene,
            assets.earthDiffuse,
            assets.earthBump,
            assets.earthSpecular,
            assets.cityEmissive,
            assets.cityShaders,
            assets.atmosShaders
        ));

        new Skybox(newScene);

        newCamera.position.set(0, 0, -5);
        newCamera.lookAt(0, 0, 0);

        const directionalLight = new T3.DirectionalLight(0xffffff, 1);
        directionalLight.position.set(-10, 4, 0);
        directionalLight.castShadow = true;
        directionalLight.shadow.mapSize.width = 2048; // default
        directionalLight.shadow.mapSize.height = 2048; // default
        newScene.add(directionalLight);

        document.addEventListener('mousemove', handleMouseMove);

    }, [
        canvas, 
        handleMouseMove,
        assets.atmosShaders, 
        assets.cityEmissive, 
        assets.cityShaders, 
        assets.earthBump, 
        assets.earthDiffuse, 
        assets.earthSpecular
    ]);

    useImperativeHandle(ref, () => ({update}))

    const update = (deltaTime: number) : void => {
        if(earth){
            earth.setRotation(normalisedMousePos);
        }
        if(scene && camera){
            renderer.render(scene, camera);
        }
    };

    return null;
});

export default AboutScene;
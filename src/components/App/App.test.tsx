import React from 'react';
import { render, screen  } from '@testing-library/react';
import App from 'components/App/App';

test('renders app container', () => {
  render(<App />);
  const container = screen.getByTestId("app-container");
  expect(container).toBeInTheDocument();
});

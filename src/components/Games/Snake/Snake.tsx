import React from 'react';

import classes from './Snake.module.css'

class Snake extends React.Component {
    render(): React.ReactNode {
        return (
            <div className={classes.Container}>
                <p>SNAKE</p>
            </div>
        )
    }
}

export default Snake;
import earthFrag from './earth.frag';
import earthVert from './earth.vert';

import citiesFrag from './cities.frag';
import citiesVert from './cities.vert';

import atmosphereFrag from './atmosphere.frag';
import atmosphereVert from './atmosphere.vert';

const earthShaders = {
    Earth: {
        fragment: earthFrag,
        vertex: earthVert
    },
    Cities: {
        fragment: citiesFrag,
        vertex: citiesVert
    },
    Atmosphere: {
        fragment: atmosphereFrag,
        vertex: atmosphereVert
    }
}

export default earthShaders;
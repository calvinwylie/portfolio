import { useState, useEffect } from "react";

const promiseWrapper = <T>(promise: Promise<T>) => {
  let status = "pending";
  let result: T;

  const s = promise.then(
    (value) => {
      status = "success";
      result = value;
    },
    (error) => {
      status = "error";
      result = error;
    }
  );

  return () => {
    switch (status) {
      case "pending":
        throw s;
      case "success":
        return result;
      case "error":
        throw result;
      default:
        throw new Error("Unknown status");
    }
  };
};

function useLoadAssets(assetRequests: Promise<any>[]) {
  const [resource, setResource] = useState(null);

  useEffect(() => {
    if(resource) return;

    const getData = async () => {
        let promise = Promise.all(assetRequests);
        setResource(promiseWrapper<any>(promise));
    };
        getData();
    
  }, [resource, assetRequests]);

  return resource;
}

export default useLoadAssets;

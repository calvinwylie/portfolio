struct DirectionalLight {
    vec3 color;
    vec3 direction;
};

uniform sampler2D texture1;
uniform DirectionalLight directionalLights[1];

varying vec2 vUv;
varying vec4 vNormal;
varying vec4 vPosition; 

void main() {
    vec4 textureColor = texture2D(texture1, vUv);

    vec3 L = normalize(- directionalLights[0].direction);
    vec3 N = normalize(vNormal.xyz);

    float intensity_unit = dot(N, L) + 0.025;
    float intensity = intensity_unit * 1.5;

    intensity = smoothstep(0.0, 0.25, intensity);

    gl_FragColor = vec4(
        textureColor.r * directionalLights[0].color.r * intensity, 
        textureColor.g * directionalLights[0].color.g * intensity, 
        textureColor.b * directionalLights[0].color.b * intensity, 
        textureColor.r * intensity
    );
}
             
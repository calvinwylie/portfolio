import React, { FC, useState } from 'react';
import classes from "./AboutPage.module.css";
import Engine from 'engine/Engine';
import Nav from 'components/Partials/Nav/Nav';

import 'mapbox-gl/dist/mapbox-gl.css';

import EarthShaders from 'assets/shaders/Earth';

import EarthLandOcean from 'assets/textures/Earth/worldmask_pacific_2048x1024_fixed.jpg';
import EarthEmissive from 'assets/textures/Earth/earth_nightlights_5k.png';

import {textureLoader, shaderLoader} from 'engine/util/Loaders'
import useLoadAssets from 'components/_Utils/UseLoadAssets';
import BioScene from 'engine/scenes/MenuScenes/BioScene';
import Button from 'components/_Lib/Button/Button';

const locations = [
    "South Africa",
    "United Kingdom",
    "Germany",
    "Portugal",
    "NoLoc"
] as const;

type Location = typeof locations[number];
type LocMetadata = {
    latLong: [number, number];

}


const locationMetadata: {[key in Location]: LocMetadata} = {
    "South Africa": {
        latLong: [-30.12, 24.04]
    },
    "United Kingdom":{
        latLong: [54.05, -2.09]
    },
    "Germany": {
        latLong: [50.79, 10.0]
    }, 
    "Portugal": {
        latLong: [39.56, -8.14]
    },
    "NoLoc": {
        latLong: [0.0, 0.0]
    }
}


const StoryPage: FC<{}> = (props) =>  {

    const assets = useLoadAssets([
        textureLoader.loadAsync(EarthEmissive),
        textureLoader.loadAsync(EarthLandOcean),
        shaderLoader.loadAsync(EarthShaders.Cities.vertex),
        shaderLoader.loadAsync(EarthShaders.Cities.fragment)
    ]);

    let [selectedLocation, setSelectedLocation] = useState<Location>("NoLoc");

    return (
        <>
            <div className={classes.Container}>
                <div className={classes.UIOverlay}>
                    <Nav/>
                    <div className={classes.CanvasContainer}>
                        {assets && 
                            <Engine 
                                clearColor={0x000000}
                                renderFunction={(canvas, renderer, ref) => {
                                    return (
                                        <BioScene 
                                            ref={ref} 
                                            canvas={canvas} 
                                            renderer={renderer} 
                                            assets={{
                                                earthSpecular: assets[1],
                                                cityEmissive: assets[0],
                                                cityShaders: [assets[2], assets[3]]
                                            }}
                                            latLong={locationMetadata[selectedLocation].latLong}
                                        />
                                    )
                                }}

                            />
                        }
                    </div>
                    <div className={classes.Bio}>
                        <h1>About Me</h1>
                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Magnam perspiciatis cumque sequi tempora recusandae consequatur
                            harum nam voluptate magni, nostrum veniam. Nam, assumenda odit
                            perspiciatis vel doloribus dolor fugiat magni!
                        </p>
                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Magnam perspiciatis cumque sequi tempora recusandae consequatur
                            harum nam voluptate magni, nostrum veniam. Nam, assumenda odit
                            perspiciatis vel doloribus dolor fugiat magni!
                        </p>
                        {locations.map((loc) => {
                            if(loc === "NoLoc") return null;
                            return (
                                <Button 
                                    active={selectedLocation === loc}
                                    onClick={() => {
                                        setSelectedLocation(loc)
                                    }}
                                > 
                                    {loc}
                                </Button>
                            )
                        })}
                    </div>
                </div>
            </div>
        </>
    )
    
    
};

export default StoryPage;
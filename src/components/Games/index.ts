import {lazy, LazyExoticComponent} from 'react';
import { Dict } from 'utils/types';

interface GameMetadata {
    image: string;
    component: LazyExoticComponent<any>;
    name: string;
}

const GameIndex: Dict<GameMetadata> = {
    snake: {
        image: "",
        component: lazy(() => import("components/Games/Snake/Snake")),
        name: "Snake"
    },
    chopper: {
        image: "",
        component: lazy(() => import("components/Games/Chopper/Chopper")),
        name: "Chopper"
    },
    runner: {
        image: "",
        component: lazy(() => import("components/Games/RaceTheSun/RaceTheSun")),
        name: "Race The Sun"
    }

}

export default GameIndex;
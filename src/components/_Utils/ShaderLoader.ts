import axios from "axios"

class ShaderLoader {
    loadAsync(url: string){
        return new Promise<string>((resolve, reject) => {
            axios.get(url).then(response => resolve(response.data));
        })
    }
}

export default ShaderLoader;
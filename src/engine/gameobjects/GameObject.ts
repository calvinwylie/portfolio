import * as T3 from 'three'

export default abstract class GameObject {
    protected mesh!: T3.Mesh;

    getScale()    : T3.Vector3 {return this.mesh.scale;}
    getMesh()     : T3.Mesh    {return this.mesh}
    getPosition() : T3.Vector3 {return this.mesh.position;}
}
import {StoryStep} from './AboutPageTypes';
import moment from 'moment';
import { FeatureCollection } from 'geojson';

export const storyConfig: Array<StoryStep> = [
    {
        tags: ["life event"],
        title: "Life begins",
        startdate: "05/1991", 
        location: "Johannesburg, South Africa",
        position: [28.0340, -26.1952]
    },
    {
        tags: ["life event"],
        title: "Moved to the Coast",
        startdate: "01/1992", 
        location: "Plettenberg Bay, South Africa",
        position: [34.0562, -23.3703]
    },
    {
        tags: ["education"],
        title: "Primary School",
        establishment: "Plettenberg Bay Primary School",
        startdate: "01/1998", 
        location: "Plettenberg Bay, South Africa",
        position: [34.0562, -23.3703]
    },
    {
        tags: ["life event"],
        title: "Emigrated to Scotland",
        startdate: "05/2002", 
        location: "Aviemore, Scotland",
        position: [-3.8291, 57.1966]
    },
    {
        tags: ["education"],
        title: "High School",
        establishment: "Kingussie High School",
        startdate: "09/2003", 
        location: "Kingussie, Scotland",
        position: [-4.0517, 57.0775]
    },
    {
        tags: ["education"],
        title: "BSc Computer Games Tech",
        establishment: "University of Abertay",
        startdate: "09/2010", 
        enddate: "05/2014",
        location: "Dundee, Scotland",
        position: [-2.9737, 56.4631]
    },
    {
        tags: ["career"],
        title: "Junior Games Developer",
        establishment: "Inspired Gaming Group",
        startdate: "08/2016",
        enddate: "08/2017",
        location: "Birmingham, England",
        position: [-1.9026, 52.4844]
    },
    {
        tags: ["career"],
        title: "Junior Web Developer",
        establishment: "British Software",
        startdate: "08/2017", 
        enddate: "11/2017",
        location: "Bournemouth, England",
        position: [-1.8650, 50.7254]
    },{
        tags: ["career"],
        title: "Freelance Developer",
        establishment: "Various",
        startdate: "11/2017", 
        enddate: "08/2018",
        location: "Bournemouth, England",
        position: [-1.8650, 50.7254]
    },
    {
        tags: ["career"],
        title: "Web Developer",
        establishment: "McKenzie Intelligence Service",
        startdate: "08/2018", 
        enddate: "01/2021",
        location: "Bournemouth, England",
        position: [-1.8650, 50.7254]
    },
    {
        tags: ["career"],
        title: "Senior FE Developer",
        establishment: "McKenzie Intelligence Service",
        startdate: "01/2021",
        enddate: "02/2022",
        location: "Bournemouth, England",
        position: [-1.8650, 50.7254]
    },
    {
        tags: ["career"],
        title: "Lead Developer",
        establishment: "McKenzie Intelligence Service",
        startdate: "02/2022", 
        enddate: moment().format("MM/YYYY"),
        location: "Bournemouth, England",
        position: [-1.8650, 50.7254],
        path: "/markdown/mckenzie_lead.md"
    }
];

export const POIs: FeatureCollection = {
    type: "FeatureCollection",
    features: storyConfig.map((storyItem) => {
        return {
            type: "Feature",
            properties: {},
            geometry: {
                type: "Point",
                coordinates: storyItem.position,
            }
        }
    })
}
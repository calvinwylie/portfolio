import { blueMaterial, greyMaterial } from "engine/util/materials";
import * as T3 from "three";
import GameObject from "../GameObject";


const MAX_SPEED = 15;

export default class Player extends GameObject {
   
    private energyMultiplier: number = 0.01;
    private power: number = 4;
    private drag: number = 0.5;

    private colliderMesh: T3.Mesh;

    private velocity: T3.Vector3;
    private camera: T3.PerspectiveCamera;
    private TPSCameraPos: T3.Vector3;
    private TDCameraPos: T3.Vector3;

    thirdPerson: boolean = true; 

    constructor(camera: T3.PerspectiveCamera) {
        super();
        
        const playerGeom = new Float32Array( [
            0.0,  0.5, -0.5,    // v0
            2.0,  0.0, -1.0,    // v1
            0.0,  0.0,  1.0,    // v2
            -2.0, 0.0, -1.0,    // v3
            0.0, -0.5,  -0.5,    // v4
        ] );
        
        
        const indices = [
            0, 2, 1,
            0, 3, 2,
            3, 0, 4,
            0, 1, 4,
        ];
        
        const geometry = new T3.BufferGeometry();
        geometry.setIndex( indices );
        geometry.setAttribute( 'position', new T3.BufferAttribute( playerGeom, 3 ) );
        geometry.computeVertexNormals();
        geometry.scale(0.125, 0.125, 0.125);
        
        this.mesh = new T3.Mesh( geometry, greyMaterial);
        this.mesh.position.set(0, 0.25, 0);

        this.mesh.castShadow = true;
        

        const collider = new T3.SphereGeometry(0.25);
        this.colliderMesh = new T3.Mesh(collider, blueMaterial);

        this.velocity = new T3.Vector3(0, 0, MAX_SPEED);

        this.camera = camera;
        this.TPSCameraPos = new T3.Vector3(0, 0.22, -1.25);
        this.TDCameraPos = new T3.Vector3(0, 60, -45);

    }

    getMesh(): T3.Mesh {return this.mesh}
    getColliderMesh(): T3.Mesh{return this.colliderMesh}

    moveLeft(): void { 
        this.velocity.x = this.velocity.x + this.power > MAX_SPEED? MAX_SPEED : this.velocity.x + this.power;
    }
    moveRight(): void { 
        this.velocity.x = this.velocity.x - this.power < -MAX_SPEED? MAX_SPEED : this.velocity.x - this.power;
    }
    moveBackward() : void { this.velocity.z -= this.velocity.z < -MAX_SPEED? 0.0 : this.power;}
    moveForward()  : void { this.velocity.z += this.velocity.z >  MAX_SPEED? 0.0 : this.power;}
    jump()         : void { this.velocity.y = 10;} 

    update(deltaTime: number, energyCount: number){
        this.velocity.x *= 1 - this.drag;
        if(this.mesh.position.y >= 0.25){
            this.velocity.y -= 9.8 * deltaTime;
        }else{
            this.mesh.position.y = 0.25;
            this.velocity.y = 0;
        }
        // this.velocity.z *= 1 - this.drag;

        
        this.mesh.rotation.z = -Math.PI * (this.velocity.x / MAX_SPEED);

        this.mesh.position.x += this.velocity.x * deltaTime;
        this.mesh.position.y += this.velocity.y * deltaTime;
        this.mesh.position.z += this.velocity.z * deltaTime;

        this.colliderMesh.position.x = this.mesh.position.x
        this.colliderMesh.position.y = this.mesh.position.y
        this.colliderMesh.position.z = this.mesh.position.z
        
        this.camera.position.x = this.mesh.position.x + (this.thirdPerson? this.TPSCameraPos.x: this.TDCameraPos.x);
        this.camera.position.y = this.mesh.position.y + (this.thirdPerson? this.TPSCameraPos.y: this.TDCameraPos.y);
        this.camera.position.z = this.mesh.position.z + (this.thirdPerson? this.TPSCameraPos.z: this.TDCameraPos.z);

        this.camera.lookAt(this.mesh.position.x, this.mesh.position.y, this.mesh.position.z + 5);
    }

}
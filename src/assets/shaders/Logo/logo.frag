uniform sampler2D texture1;
uniform sampler2D texture2;

uniform float screenWidth;
uniform float screenHeight;

uniform vec3 color;

varying vec2 vUv;
varying vec3 vNormal;

void main() {
    vec4 outline = texture2D(texture1, vUv); 
    // if(length(outline.xyz) < 0.5){
    //     gl_FragColor = vec4(color, 0.6);
    // }else{
        gl_FragColor = texture2D(
            texture2, 
            vec2(gl_FragCoord.x/screenWidth, gl_FragCoord.y/screenHeight)
        );
    // }
}
interface Timer {
    active: boolean;
    initialInterval: number;
    interval: number;
    recurring: boolean;
    func: (...args: any) => any;
}

export default class Clock {
    private timers: Timer[] = [];
    elapsedTime: number = 0;

    newTimer(func: (...args: any) => any, interval: number, recurring: boolean = false) {

        let inactiveTimer = this.timers.find(timer => !timer.active)

        if(inactiveTimer){
            inactiveTimer.initialInterval = interval;
            inactiveTimer.interval= interval;
            inactiveTimer.recurring = recurring;
            inactiveTimer.func = func;
            inactiveTimer.active = true;
            
        }else{
            this.timers.push({
                initialInterval: interval,
                interval,
                recurring,
                func,
                active: true
            });
        }


    }

    update(deltaTime: number){

        this.elapsedTime += deltaTime/1000;

        for(let timer of Object.values(this.timers).filter(timer => timer.active)){
            timer.interval -= deltaTime;
            if(timer.interval < 0){
                if(timer.recurring){
                    timer.interval = timer.initialInterval;
                }else{
                    timer.active = false;
                }
                timer.func();
            }
        }
    }

}
import React, {FC} from 'react';

import classes from './Loading.module.css'

const Loading: FC = () => {
    return (
        <div className={classes.IconContainer}>
            <div className={classes.Icon}/>
            <div className={classes.Icon}/>
        </div>
    )
};

export default Loading;
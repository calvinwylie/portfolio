import * as T3 from "three";

import GameObject from "../GameObject";

const EARTH_SIZE_MULTIPLIER = 2;
const ANIMATION_TIME = 0.5;

export default class SpotEarth extends GameObject {

    earth?: T3.Object3D;
    animating: boolean = false;
    targetLatLong: T3.Vector2 = new T3.Vector2(0, 0);
    currentLatLong: T3.Vector2 = new T3.Vector2(0, 0);
    animationTime: number = 0;

    constructor(
        scene: T3.Scene,
        earthSpecular: T3.Texture
    ) {
        super();

        const earthMaterial = new T3.MeshLambertMaterial({
            map: earthSpecular,
            specularMap: earthSpecular
        });
        const earthGeom = new T3.SphereGeometry(EARTH_SIZE_MULTIPLIER, 50, 50);
        const earthMesh = new T3.Mesh(earthGeom, earthMaterial);
        this.earth = earthMesh;

        scene.add(earthMesh);
    }

    getPosition(): T3.Vector3 {
        return this.earth?.position ?? new T3.Vector3(5);
    }

    setTargetRotation(latLong: [number, number]) {
        if(
            latLong[0] !== this.currentLatLong.x && 
            latLong[1] !== this.currentLatLong.y 
        ){
            this.targetLatLong = new T3.Vector2(latLong[0], latLong[1]);
            this.animating = true;
            this.animationTime = 0;
        }
    }

    rotate(mouseMovementX: number, mouseMovementY: number){
        if(this.earth){
            this.currentLatLong.set(
                this.currentLatLong.x + mouseMovementY/10,
                this.currentLatLong.y - mouseMovementX/10,
            );
        }
    }

    update(deltaTime: number){

        if(this.animating){
            this.animationTime += deltaTime;
            this.currentLatLong.lerp(this.targetLatLong, this.animationTime/ANIMATION_TIME);
        }

        if(this.earth){
            this.earth.rotation.z = T3.MathUtils.degToRad(this.currentLatLong.x);
            this.earth.rotation.y = -T3.MathUtils.degToRad(this.currentLatLong.y) - Math.PI/2;
        }

        
    }
}
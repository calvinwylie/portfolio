import { lazy } from "react";


export function lazy_delay(component_path: any, delay: number){
    return lazy(() => {
        return new Promise((resolve) => {
            setTimeout(() => {resolve(component_path)}, delay);
        })
    });
}

import React, {useEffect, lazy, Suspense, useState, createContext} from 'react';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";

import Tooltip from "components/_Lib/Tooltip/Tooltip";
import LoadingPage from "components/Pages/LoadingPage/LoadingPage";

import classes from './App.module.css';
import Nav from 'components/Partials/Nav/Nav';

const HomePage = lazy(() => import("components/Pages/HomePage/HomePage"));
const StoryPage = lazy(() => import("components/Pages/AboutPage/AboutPage"));
const GameGalleryPage = lazy(() => import("components/Pages/GameGalleryPage/GameGalleryPage"));
const GamePage = lazy(() => import("components/Pages/GamePage/GamePage"));


interface ContextInterface<T> {
    value: T;
    setState: React.Dispatch<React.SetStateAction<T>>
}

export const ThemeContext = createContext<ContextInterface<string>>({
    value: 'dark',
    setState: () => {}
});

export const NavContext = createContext<ContextInterface<boolean>>({
    value: false,
    setState: () => {}
});


function App() {

    useEffect(() => {}, []);

    const [navOpen, setNavOpen] = useState<boolean>(false);

    return (
        <NavContext.Provider value={{
            value: navOpen,
            setState: setNavOpen
        }}>
            <div className={classes.Container} data-testid={"app-container"}>
                <BrowserRouter>
                    <Suspense fallback={<LoadingPage/>}>
                        <Routes>
                            <Route path="/" element={<HomePage/>}/>
                            <Route path="/story" element={<StoryPage/>}/>
                            <Route path="/game_gallery" element={<GameGalleryPage/>}/>
                            <Route path="/game/:game" element={<GamePage/>}/>
                        </Routes>
                    </Suspense>
                    <Tooltip/>
                    <Nav/>
                </BrowserRouter>
            </div>
        </NavContext.Provider>
    );
}


export default App;

import * as T3 from 'three'
import {debounce } from 'debounce';

import { EngineScene } from 'engine/EngineScene';
import Player from 'engine/gameobjects/RaceTheSun/Player';
import { HUDData } from 'components/Games/RaceTheSun/RaceTheSun';
import Terrain from 'engine/gameobjects/RaceTheSun/Terrain';
import { loadPatterns } from 'engine/util/segmentLoader';
import Sun from 'engine/gameobjects/RaceTheSun/Sun';
import Skybox from 'engine/gameobjects/RaceTheSun/Skybox';


export const SPAWN_X_SPREAD = 20; 
export const SPAWN_Y_SPREAD = 400; 
export const TRI_PER_MULTI = 5;
const BASE_FRAME_SCORE = 5;



export default class RTC_MainScene extends T3.Scene implements EngineScene {

    updateDisplay : (update: Partial<HUDData>) => void;
    toggleDebugInfo : () => void;
    
    private triCount = 0;
    private terrain: Terrain | null = null;
    private camera: T3.PerspectiveCamera | undefined;
    private spareVector: T3.Vector3 = new T3.Vector3();
    //@ts-ignore: Player is initialised in the init function during engine startup.
    player: Player;
    private sun: Sun;
    private skybox: Skybox;

    private score: number = 0;
    private multiplier: number = 1;

    private keyDown: Set<string>;



    constructor(updateDisplay: (update: Partial<HUDData>) => void, toggleDebugInfo: () => void) {
        super();
        loadPatterns(this.createTerrain);

		this.fog = new T3.Fog( 0xCCCCCC, 1, 100 );

        this.toggleDebugInfo = debounce(toggleDebugInfo, 200, true);
        this.updateDisplay = updateDisplay;
        this.keyDown = new Set<string>();

        // -- SKYBOX
        this.skybox = new Skybox(this);

        // -- SUN
        this.sun = new Sun();
        this.add(this.sun.mesh);
        this.add(this.sun.light);

         const ambientLight = new T3.AmbientLight(0xffffff, 0.2);
         this.add(ambientLight);
 
         document.addEventListener("keydown", this.handleKeyDown);
         document.addEventListener("keyup", this.handleKeyUp);
    }

    init(canvas: HTMLCanvasElement){
        // -- CAMERA
        this.camera = new T3.PerspectiveCamera(50, canvas.width/canvas.height, 0.1, 200);
        this.camera.position.set(0, 4, -5);
        this.camera.lookAt(0, 4, 0);

        //  -- PLAYER
        this.player = new Player(this.camera);
        this.add(this.player.getMesh());
    }

    createTerrain = () => {
        this.terrain = new Terrain(this, this.player);
    }
    
    
    addToEnergyCount = (delta: number) => {
        this.triCount += delta;

        if(delta > 0){
            this.sun.raiseSun()
            this.multiplier = 1 + (this.triCount / (TRI_PER_MULTI + 1));
        }else{
            this.sun.lowerSun()
        }

        this.updateDisplay({
            triCount: this.triCount,
            multiplier: this.multiplier
        });
    }

    private handleKeyUp = (event: KeyboardEvent) => {
        this.keyDown.delete(event.key.toLowerCase());
    }

    private handleKeyDown = (event: KeyboardEvent) => {
        this.keyDown.add(event.key.toLowerCase());
    }

    toggleCamera() {
        this.player.thirdPerson = !this.player.thirdPerson;
    }

    updateInput(){
        if(this.keyDown.has("w")){
            // this.player.moveForward()
        }else if (this.keyDown.has("s")){
            // this.player.moveBackward()
        }
        
        if (this.keyDown.has("d")){
            this.player.moveRight()
        }else if (this.keyDown.has("a")){
            this.player.moveLeft()
        }

        if (this.keyDown.has(" ")){
            this.player.jump()
        }

        if (this.keyDown.has("`")){
            this.toggleDebugInfo();
        }
    }

    update(deltaTime: number){

        this.score += BASE_FRAME_SCORE * this.multiplier; 

        this.updateDisplay({
            location: this.player.getPosition(),
            score: this.score
        })
        this.updateInput()

        this.sun.update(deltaTime, this.player);
        this.skybox.update(deltaTime, this.player);
        this.terrain?.update(this.addToEnergyCount);
        this.player.update(deltaTime, this.triCount);
        
        if(this.camera){
            this.camera.getWorldDirection(this.spareVector)
        }

    }

    render(renderer: T3.Renderer) {
        if(this.camera){
            renderer.render(this, this.camera);
        }
    }
}
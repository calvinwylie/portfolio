import * as T3 from "three";
import GameObject from "../GameObject";


export default class Logo extends GameObject {

    object?: T3.Object3D;

    constructor(
        scene: T3.Scene, 
        canvas: HTMLCanvasElement, 
        logoModel: T3.Group,
        logoTexture: T3.Texture,
        backgroundTexture: T3.Texture,
        logoShaders: [string, string],
        bgColor: number
    ) {
        super();

        let uniforms = {
            texture1: { type: "t", value: logoTexture },
            texture2: { type: "t", value: backgroundTexture },
            screenWidth: { type: 'f', value: canvas.offsetWidth },
            screenHeight: { type: 'f', value: canvas.offsetHeight},
            color: {value: new T3.Color(bgColor)}
        };

        const logoMaterial = new T3.ShaderMaterial ({
            uniforms: uniforms,
            vertexShader: logoShaders[0],
            fragmentShader: logoShaders[1]
        })

        let object = logoModel;
        
        object.traverse( function ( child ) {
            if ( child instanceof T3.Mesh ) {
                child.material = logoMaterial;
            }
            scene.add(object);
        });   
        object.scale.set(1.1, 1.1, 1.1);
        this.object = object;
    }

    getPosition(): T3.Vector3 {
        return this.object?.position ?? new T3.Vector3(5);
    }

    setRotation(normalisedMousePos: [number, number]) {
        if(this.object){
            this.object.rotation.x = -(Math.PI / 4 * (normalisedMousePos[1] - 0.5));
            this.object.rotation.y = Math.PI / 4 * (normalisedMousePos[0] - 0.5);
        }
    }

    update(){}

}
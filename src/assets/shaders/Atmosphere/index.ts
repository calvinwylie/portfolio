import fragment from './atmosphere.frag';
import vertex from './atmosphere.vert';

export default {
    fragment,
    vertex
}
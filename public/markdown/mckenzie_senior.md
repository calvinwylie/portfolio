Leading the tech deparment was a huge learning experience for me. Moving further from the day to day
development and moving towards a more strategic position. Having to manage 6 staff as well as multiple
verticals of software development. 

## Accomplishments

- Developed the React SPA front-end to replace the SSR php portal.    
- Refactored the companies internal tooling into SPA website for a better internal user experience.  

## Responsibilities

- Code review and release of back and front end software. 
- Architecture Design and implmentation of cloud systems, databases and data storage.   
- Ideation and tech input into product development.
- Mentorship of junior and new tech members. 
- Internal tooling for Intelligence Team. 
- Repo and CICD management. 
- Managing the budget and accounts for the tech department. 


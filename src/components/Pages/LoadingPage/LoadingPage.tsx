import React, {FC} from 'react';

import Loading from "components/Partials/Loading/Loading";

import classes from './LoadingPage.module.css';
import Engine from 'engine/Engine';
import LoadingScene from 'engine/scenes/MenuScenes/LoadingScene';

const LoadingPage: FC = () => {
    return (
        <>
            <Engine 
                clearColor={0x000000}
                renderFunction={(canvas, renderer, ref) => {
                    return (
                        <LoadingScene 
                            ref={ref} 
                            canvas={canvas} 
                            renderer={renderer}
                        />
                    )
                }}
            />            
            <div className={classes.Container}>
                <Loading/>
            </div>
        </>
    )
};

export default LoadingPage;
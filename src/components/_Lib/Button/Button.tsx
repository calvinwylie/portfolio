import { FC, PropsWithChildren } from 'react';
import cx from 'classnames'
import classes from './Button.module.css';



type ButtonProps = PropsWithChildren<{
    onClick: () => void;
    active?: boolean;
}>

const Button: FC<ButtonProps> = ({children, onClick, active}: ButtonProps) => {
    return (
        <button 
            onClick={onClick}
            className={cx(classes.Container, {[classes.Active]: active})}
        >
            {children}
        </button>
    )
}

export default Button;
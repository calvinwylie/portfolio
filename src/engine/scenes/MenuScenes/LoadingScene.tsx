import { SceneProps } from 'engine/EngineScene';
import * as T3 from 'three';
import { forwardRef, useCallback, useEffect, useRef, useState, useImperativeHandle } from 'react';


interface LoadingSceneProps extends SceneProps {}

const LoadingScene = forwardRef(({canvas, renderer, assets}: LoadingSceneProps, ref) => {

    let [scene, setScene] = useState<T3.Scene | null>(null);
    let [camera, setCamera] = useState<T3.Camera | null>(null);
    let [particles, setParticles] = useState<T3.Points | null>();
    
    const [mousePos, setMousePos] = useState<[number, number]>([0.5, 0.5]);
    
    let canvasRect = useRef(canvas.getBoundingClientRect());

    let [bufferScene, setBufferScene] = useState<T3.Scene | null>(null);
    let [frameBuffer, setFrameBuffer] = useState<T3.WebGLRenderTarget | null>(null);
    let [frameBuffer2, setFrameBuffer2] = useState<T3.WebGLRenderTarget | null>(null);
    let [mousePosPlane, setMousePosPlane] = useState<T3.Mesh | null>(null);
    let [prevPlane, setPrevPlane] = useState<T3.Mesh | null>(null);
    let [flipFlop, setFlipFlop] = useState<boolean>(false);
    
    const handleMouseMove = useCallback((mouseEvent: MouseEvent & {layerX?: number, layerY?: number}) => {
        if(mouseEvent.layerX && mouseEvent.layerY){
            setMousePos([
                mouseEvent.layerX - canvasRect.current.width/2,
                -mouseEvent.layerY + canvasRect.current.height/2
            ]);
        }else{
            setMousePos([
                (mouseEvent.clientX - canvasRect.current.left), 
                -(mouseEvent.clientY - canvasRect.current.top)
            ]);
        }
    }, []);

    useEffect(() => {

        const scene_init = new T3.Scene();
        const bufferScene_init = new T3.Scene();

        setScene(scene_init);
        setBufferScene(bufferScene_init);
 
        if(canvas){

            let width = canvas.offsetWidth;
            let height = canvas.offsetHeight;

            setFrameBuffer(new T3.WebGLRenderTarget(width, height));
            setFrameBuffer2(new T3.WebGLRenderTarget(width, height));

            let mousePosMaterial: T3.Material = new T3.ShaderMaterial({
                uniforms: {
                    mousePos: {value: new T3.Vector2(width/2, height/2)}
                },
                vertexShader: `
                    varying vec2 vPos;
                    void main(){
                        vPos = position.xy;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                    }
                `,
                fragmentShader: `
                    varying vec2 vPos;
                    uniform vec2 mousePos;

                    void main(){
                        float color = smoothstep(150.0, 0.0, length(vPos - mousePos));
                        gl_FragColor = vec4(0.0, 0.0, 1.0, color);
                    }
                `,
                transparent: true
            });

            const mousePosPlane_init = new T3.Mesh(new T3.PlaneGeometry(width, height), mousePosMaterial);
            mousePosPlane_init.position.set(0, 0, -1);
            setMousePosPlane(mousePosPlane_init);
            bufferScene_init.add(mousePosPlane_init);

            const fadePlane = new T3.Mesh(new T3.PlaneGeometry(width, height), new T3.MeshBasicMaterial({
                color: new T3.Color(0x000000),
                opacity: 0.03,
                transparent: true
            }));
            fadePlane.position.set(0, 0, -2);
            bufferScene_init.add(fadePlane);

            const prevPlane_inst = new T3.Mesh(new T3.PlaneGeometry(width, height), new T3.MeshBasicMaterial({
                map: new T3.DataTexture(new Uint16Array(4), width, height)
            }))
            prevPlane_inst.position.set(0, 0, -3);
            bufferScene_init.add(prevPlane_inst);
            setPrevPlane(prevPlane_inst);

            let camera_init: T3.Camera = new T3.OrthographicCamera(-width/2, width/2, height/2, -height/2, 0.1, 1000);
            setCamera(camera_init);

            let seperation = 20;

            let numParticlesX = Math.floor(width/seperation) + 1;
            let numParticlesY = Math.floor(height/seperation) + 1;

            let numParticles = numParticlesX * numParticlesY;

            let positions = new Float32Array(numParticles * 3);

            let i = 0;

            for ( let ix = 0; ix < numParticlesX; ix ++ ) {
                for ( let iy = 0; iy < numParticlesY; iy ++ ) {

                    positions[ i ] = ix * seperation - ( ( numParticlesX * seperation ) / 2 ); // x
                    positions[ i + 1 ] = iy * seperation - ( ( numParticlesY * seperation ) / 2 ); // y
                    positions[ i + 2 ] = -1;

                    i += 3;
                }
            }

            let geometry = new T3.BufferGeometry();
            geometry.setAttribute('position', new T3.BufferAttribute(positions, 3));

            let uniforms = {
                texture1: {type: "t", value: new T3.DataTexture(new Uint16Array(4), 2, 2)}
            }

            const material = new T3.ShaderMaterial( {
                uniforms,
                vertexShader: `
                    uniform sampler2D texture1;
                    varying float vScale;

                    void main() {
                        vec4 recencyColor = texture2D(texture1, vec2((position.x/${width}.0) + 0.5, (position.y/${height}.0) + 0.5));
                        vScale = 13.0 * recencyColor.b;
                        gl_PointSize = vScale;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                    }
                `,
                fragmentShader: `
                    uniform sampler2D texture1;
                    varying float vScale;

                    vec3 white = vec3(1., 1., 1.);
                    vec3 blue = vec3(0., 0.3, 0.8);
                    vec3 orange = vec3(0.6, 0.4, 0.);

                    float step1 = 10.0;
                    float step2 = 15.0;


                    void main() {
                        if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.6 ) discard;

                        vec3 color = mix(blue, orange, smoothstep(0.0, step1, vScale));
                        color = mix(color, white, smoothstep(step1, step2, vScale));

                        gl_FragColor = vec4(color, 1.);
                    }
                `
            });

            let particles = new T3.Points( geometry, material );
            setParticles(particles);
            scene_init.add( particles );
        }

        document.addEventListener('mousemove', handleMouseMove);

    }, [canvas, handleMouseMove]);

    useImperativeHandle(ref, () => ({update}))

    const update = (deltaTime: number) : void => {

        if(mousePosPlane){
            (mousePosPlane.material as T3.ShaderMaterial).uniforms.mousePos.value = new T3.Vector2(mousePos[0], mousePos[1]);
        }

        if(
            canvas &&  camera &&
            frameBuffer && frameBuffer2 && 
            bufferScene && scene &&
            prevPlane && particles
        ){ 
        
            let activeFrameBuffer = flipFlop? frameBuffer: frameBuffer2;

            renderer.setRenderTarget(activeFrameBuffer);
            renderer.render(bufferScene, camera);
            (prevPlane.material as T3.MeshBasicMaterial).map = activeFrameBuffer.texture;

            (particles.material as T3.ShaderMaterial).uniforms.texture1.value = activeFrameBuffer.texture;
            (particles.material as T3.ShaderMaterial).needsUpdate = true;

            renderer.setRenderTarget(null);
            renderer.render(scene, camera);

            setFlipFlop(!flipFlop)
        }
    };

    return null;
});

export default LoadingScene;
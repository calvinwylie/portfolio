import * as T3 from 'three'

export const redMaterial = new T3.MeshPhongMaterial({ color: 0xFF1100});
export const blueMaterial = new T3.MeshPhongMaterial({ color: 0x0000FF});
export const yellowMaterial = new T3.MeshPhongMaterial({ color: 0xFFAD00});
export const whiteMaterial = new T3.MeshPhongMaterial({ color: 0xFFFFFF});
export const lightGreyMaterial = new T3.MeshLambertMaterial({ color: 0xbbbbbb});
export const greyMaterial = new T3.MeshLambertMaterial({ color: 0xaaaaaa});


export const blueTransparentMaterial = new T3.MeshPhongMaterial({color: 0x8cd5f7, transparent: true, opacity: 0.5})